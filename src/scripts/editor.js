/*
  Project: myAgro
  Author: Piotr Bajer
 */

import hideOn from './editor/hide-on';
import spacing from './editor/spacing';
import removeStylesBlock from './editor/remove-styles-block';
// import nowrap from './editor/nowrap';
import heading from './editor/heading';
import separatorAlignment from './editor/separator-alignment';

hideOn();
spacing();
removeStylesBlock();
// nowrap();
heading();
separatorAlignment();
