/* eslint-disable no-undef, no-unused-vars */

import classnames from 'classnames';

const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, CheckboxControl, SelectControl } = wp.components;
const { addFilter } = wp.hooks;
const { __ } = wp.i18n;

const fontWeights = [
  {
    label: __('Default'),
    value: '',
  },
  {
    label: __('Light'),
    value: 'u-fw-300',
  },
  {
    label: __('Regular'),
    value: 'u-fw-400',
  },
  {
    label: __('Medium'),
    value: 'u-fw-500',
  },
  {
    label: __('Semi bold'),
    value: 'u-fw-600',
  },
  {
    label: __('Bold'),
    value: 'u-fw-700',
  },
];

const serifClass = 'u-ff-serif';

export default () => {
  const addAttribute = (settings, name) => {
    if (name !== 'core/heading') {
      return settings;
    }

    if (settings.attributes !== undefined) {
      // eslint-disable-next-line no-param-reassign
      settings.attributes = Object.assign(settings.attributes, {
        fontSerif: {
          type: 'boolean',
          default: false,
        },
        fontWeight: {
          type: 'string',
          default: '',
        },
      });
    }

    return settings;
  };

  addFilter(
    'blocks.registerBlockType',
    'myagro/attribute/heading',
    addAttribute,
  );

  const withFontSettings = createHigherOrderComponent(
    BlockEdit => props => {
      if (props.name !== 'core/heading') {
        return <BlockEdit {...props} />;
      }

      let { className = '' } = props.attributes;
      const { setAttributes } = props;
      const fontSerif = className ? className.indexOf(serifClass) > -1 : false;
      let fontWeight = className ? className.match(/u-fw-[0-9]+/) : '';
      fontWeight = fontWeight ? fontWeight.pop() : '';

      return (
        <Fragment>
          <BlockEdit {...props} />
          <InspectorControls>
            <PanelBody title={__('Font Settings')} initialOpen={false}>
              <SelectControl
                label={__('Font Weight')}
                value={fontWeight}
                options={fontWeights}
                onChange={selectedFontWeight => {
                  className =
                    className && className.replace(/ ?u-fw-[0-9]+/g, '');
                  className = classnames(className, selectedFontWeight);
                  setAttributes({
                    fontWeight: selectedFontWeight,
                    className,
                  });
                }}
              />
              <CheckboxControl
                label="Serif Font"
                checked={fontSerif}
                onChange={value => {
                  className = className
                    .replace(serifClass, '')
                    .trim()
                    .replace('/[ ]{2,}/g', ' ');

                  if (value) {
                    className += ` ${serifClass}`;
                  }

                  props.setAttributes({
                    fontSerif: !!value,
                    className,
                  });
                }}
              />
            </PanelBody>
          </InspectorControls>
        </Fragment>
      );
    },
    'withFontSettings',
  );

  addFilter('editor.BlockEdit', 'myagro/with-font-settings', withFontSettings);
};
