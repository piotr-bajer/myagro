/* eslint-disable no-undef, no-unused-vars */

export default () => {
  wp.hooks.addFilter(
    'blocks.registerBlockType',
    'myagro/filters',
    (settings, name) => {
      if (name === 'core/separator') {
        return Object.assign({}, settings, {
          attributes: {
            ...settings.attributes,
            align: {
              type: 'string',
              default: 'full',
            },
          },
          getEditWrapperProps: () => ({ 'data-align': 'full', align: 'full' }),
        });
      }
      return settings;
    },
  );
};
