let unique = 0;

export default (url, callback, onerror) => {
  unique += 1;
  const name = `_jsonp_${unique}`;
  const parsedUrl = url.replace('c=?', `c=${name}`);
  const head = document.querySelector('head');

  let script = document.createElement('script');
  if (typeof error === 'function') {
    script.onerror = onerror;
  }

  script.type = 'text/javascript';
  script.src = parsedUrl;

  window[name] = data => {
    callback(data);
    head.removeChild(script);
    script = null;
    delete window[name];
  };

  head.appendChild(script);
};
