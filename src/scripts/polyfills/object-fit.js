export default () => {
  if ('objectFit' in document.documentElement.style === false) {
    const images = document.querySelectorAll('.u-fit-cover, .u-fit-contain');
    for (let i = 0; i < images.length; i += 1) {
      const img = images[i];
      const imageSource = img.src;
      img.style.backgroundSize =
        img.getAttribute('class').indexOf('-cover') > -1 ? 'cover' : 'contain';
      img.style.backgroundImage = `url(${imageSource})`;
      img.style.backgroundPosition = 'center center';
      img.style.backgroundRepeat = 'no-repeat';
      const w = img.getAttribute('width') || '1';
      const h = img.getAttribute('height') || '0';
      img.src = `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='${w}' height='${h}'%3E%3C/svg%3E`;
    }
  }
};
