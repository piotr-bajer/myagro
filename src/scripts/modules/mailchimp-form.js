import { on, each, setCookie } from '../helpers/shortcuts';
import Jsonp from '../helpers/jsonp';

export default () => {
  const forms = document.querySelectorAll('.js-mailchimp-form');

  if (forms.length < 1) {
    return;
  }

  each(forms, form => {
    const message = form.querySelector('.js-mailchimp-form-message');
    const submit = form.querySelector('.js-submit');

    let isSubmitting = false;
    const submitDefault = submit !== null ? submit.textContent : 'Submit';

    const setMessage = messageText => {
      if (message !== null) {
        message.textContent = messageText;
      }
    };

    const reset = () => {
      isSubmitting = false;

      if (submit !== null) {
        submit.textContent = submitDefault;
      }
    };

    on('submit', form, event => {
      event.preventDefault();

      if (isSubmitting) {
        return;
      }

      isSubmitting = true;

      setMessage('');

      if (submit !== null) {
        submit.textContent = 'Sending...';
      }

      const params = new URLSearchParams(new FormData(form)).toString();

      const url = `${`${form.action
        .replace('&c=?', '')
        .replace('/post', '/post-json')}&c=?&`}${params}`;

      Jsonp(
        url,
        data => {
          reset();

          if (data === null || typeof data !== 'object') {
            // eslint-disable-next-line no-param-reassign
            data = {};
          }

          let update = false;

          if (data.msg !== undefined && data.msg.indexOf('already') > -1) {
            update = true;
          }

          if (data.result === 'success' || update) {
            form.reset();
            setMessage(
              'You have successfully subscribed to the newsletter. Thank your for your subscription.',
            );
            setCookie('newsletter-subscribed', 1, 1825);
          } else if (data.result === 'error') {
            setMessage(
              data.msg.indexOf('Please enter a value') > -1
                ? 'Please fill in all required fields.'
                : data.msg,
            );
          } else {
            setMessage(
              'We could not subscribe your e-mail address, please try later.',
            );
          }
        },
        () => {
          reset();
          setMessage(
            'The are some server issues, we cannot subscribe you right now, please try again.',
          );
        },
      );
    });
  });
};
