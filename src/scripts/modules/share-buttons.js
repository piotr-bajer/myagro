import { on } from '../helpers/shortcuts';
import S from './shared';

export default () => {
  const sharePopupButton = document.querySelector(
    '.c-post__share-popup-button',
  );
  const bookmarkButton = document.querySelector('.js-bookmark');

  if (sharePopupButton !== null) {
    const popupContent = sharePopupButton.nextElementSibling;

    on('click', sharePopupButton, event => {
      event.stopPropagation();
      sharePopupButton.parentNode.classList.toggle(S.CSS.active);
    });

    on('click', popupContent, event => {
      event.stopPropagation();
    });

    on('click', document.documentElement, () => {
      sharePopupButton.parentNode.classList.remove(S.CSS.active);
    });
  }

  if (bookmarkButton !== null) {
    on('click', bookmarkButton, event => {
      event.preventDefault();

      const { url } = event.currentTarget.dataset;
      const { title } = document;

      if (window.sidebar && window.sidebar.addPanel) {
        window.sidebar.addPanel(title, url);
      } else if (window.external && window.external.AddFavorite) {
        window.external.AddFavorite(url, title);
      } else {
        alert(
          `You can add this page to your bookmarks by pressing ${
            navigator.userAgent.toLowerCase().indexOf('mac') !== -1
              ? 'Command/Cmd'
              : 'CTRL'
          } + D on your keyboard.`,
        );
      }
    });
  }
};
