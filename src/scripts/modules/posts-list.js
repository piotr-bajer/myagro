import { on } from '../helpers/shortcuts';

export default () => {
  const form = document.querySelector('.c-posts-list__form');

  if (form === null) {
    return;
  }

  const select = form.querySelector('.c-posts-list__select');
  const reset = form.querySelector('.c-posts-list__reset');
  const searchInput = form.querySelector('.c-posts-list__search-input');

  on('change', select, () => {
    form.submit();
  });

  on('click', reset, event => {
    event.preventDefault();
    select.value = '';
    searchInput.value = '';
    form.submit();
  });
};
