import { on } from '../helpers/shortcuts';

export default () => {
  const links = document.querySelectorAll('[href*="#"]');

  if (links.length < 1) {
    return;
  }

  on('click', links, event => {
    const bt = event.currentTarget;
    let id = bt.getAttribute('href').split('#');
    id = id.length > 1 ? id[1] : '';

    if (id) {
      const element = document.querySelector(`#${id}`);

      if (element !== null) {
        event.preventDefault();
        element.scrollIntoView({ behavior: 'smooth', block: 'start' });
      }
    }
  });
};
