import { on, each } from '../helpers/shortcuts';
import S from './shared';

export default () => {
  const selectInputs = document.querySelectorAll('.o-image-select');

  if (selectInputs.length < 1) {
    return;
  }

  each(selectInputs, selectInput => {
    const valueContainer = selectInput.querySelector('.o-image-select__value');
    const valueButton = valueContainer.querySelector('.o-image-select__option');
    const optionsContainer = selectInput.querySelector(
      '.o-image-select__options',
    );

    const setValue = button => {
      valueButton.firstElementChild.innerHTML =
        button.firstElementChild.innerHTML;
      valueButton.children[1].textContent = button.children[1].textContent;
    };

    on('click', valueContainer, event => {
      if (S.windowWidth >= S.breakpoints.large) {
        return;
      }

      event.preventDefault();
      event.stopPropagation();

      selectInput.classList.toggle(S.CSS.active);
    });

    on('click', document.documentElement, () => {
      selectInput.classList.remove(S.CSS.active);
    });

    on('click', optionsContainer.children, event => {
      if (S.windowWidth >= S.breakpoints.large) {
        return;
      }

      event.preventDefault();

      [].forEach.call(optionsContainer.children, option => {
        option.classList.remove(S.CSS.active);
      });

      event.currentTarget.classList.add(S.CSS.active);

      setValue(event.currentTarget);
    });

    on('slide-change', document, event => {
      const slideshowContainer = selectInput.closest('.js-slideshow');

      if (
        slideshowContainer !== null &&
        slideshowContainer === event.slideshowContainer
      ) {
        setValue(optionsContainer.children[event.slideIndex]);
      }
    });
  });
};
