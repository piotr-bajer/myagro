import { on } from '../helpers/shortcuts';
import S from './shared';

export default () => {
  const header = document.querySelector('.c-header');

  if (header === null) {
    return;
  }

  const hamburger = header.querySelector('.c-header__hamburger');
  const menu = header.querySelector('.c-header__menu');

  on('click', hamburger, event => {
    event.preventDefault();
    event.stopPropagation();

    hamburger.classList.toggle(S.CSS.active);
    menu.classList.toggle(S.CSS.active);
  });

  on('click', menu, event => {
    event.stopPropagation();
  });

  on('click', document.documentElement, () => {
    if (S.windowWidth <= S.breakpoints.xlarge) {
      hamburger.classList.remove(S.CSS.active);
      menu.classList.remove(S.CSS.active);
    }
  });
};
