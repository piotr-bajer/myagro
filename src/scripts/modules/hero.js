import S from './shared';

export default () => {
  const hero = document.querySelector('.c-hero');

  if (hero === null) {
    return;
  }

  const underlinedItems = hero.querySelector('.c-hero__underlined');

  if (underlinedItems.children.length < 2) {
    return;
  }

  let currentIndex = 0;
  let timeout = null;

  hero.classList.add(S.CSS.initialized);
  underlinedItems.children[currentIndex].classList.add(S.CSS.active);

  const changeText = () => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      underlinedItems.children[currentIndex].classList.remove(S.CSS.active);
      currentIndex = (currentIndex + 1) % underlinedItems.children.length;
      underlinedItems.children[currentIndex].classList.add(S.CSS.active);
      changeText();
    }, 4000);
  };

  changeText();
};
