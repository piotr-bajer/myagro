import { on, each, createEvent } from '../helpers/shortcuts';

export default () => {
  const teamGallery = document.querySelector('.c-team-gallery');

  if (teamGallery === null) {
    return;
  }

  const members = document.querySelector('.c-members');

  if (members === null) {
    return;
  }

  let allowClick = true;

  const teamButtons = document.querySelectorAll('.o-image-select__option');
  const memberButtons = document.querySelectorAll('.o-select__option');

  const click = (button, buttons, type) => {
    const textContent = button.textContent.trim().toLowerCase();

    if (!allowClick) {
      return;
    }

    allowClick = false;

    each(buttons, destButton => {
      const destTextContent = destButton.textContent.trim().toLowerCase();

      if (destTextContent && textContent && destTextContent === textContent) {
        const event = createEvent('click');

        if (type === 'member') {
          event.skipAnimation = true;
        }

        destButton.dispatchEvent(event);
      }
    });

    setTimeout(() => {
      allowClick = true;
    }, 100);
  };

  on('click', teamButtons, event => {
    click(event.currentTarget, memberButtons);
  });

  on('click', memberButtons, event => {
    click(event.currentTarget, teamButtons, 'member');
  });

  if (teamButtons.length > 0) {
    teamButtons[0].dispatchEvent(createEvent('click'));
  }
};
