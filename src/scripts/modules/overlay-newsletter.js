import S from './shared';
import { on, each, setCookie, getCookie } from '../helpers/shortcuts';
import { lockViewport, unlockViewport } from '../helpers/viewport-lock';

export default () => {
  const overlays = document.querySelectorAll('.c-overlay--newsletter');

  if (overlays.length < 1) {
    return;
  }

  each(overlays, overlay => {
    const inner = overlay.querySelector('.c-overlay__inner');
    let allowClose = false;

    const showOverlay = () => {
      if (overlay.classList.contains(S.CSS.active)) {
        return;
      }

      overlay.classList.add(S.CSS.active);
      lockViewport(overlay);
    };

    const hideOverlay = () => {
      if (!allowClose || !overlay.classList.contains(S.CSS.active)) {
        return;
      }

      overlay.classList.remove(S.CSS.active);
      unlockViewport(overlay);

      setCookie('newsletter-dismissed', 1, 30);
    };

    const closeButtons = overlay.querySelectorAll('[data-overlay-close]');
    on('click', closeButtons, event => {
      event.stopPropagation();
      event.preventDefault();
      hideOverlay();
    });

    on('click', inner, event => {
      event.stopPropagation();
    });

    on('click', overlay, () => {
      hideOverlay();
    });

    const newsletterDismissed = parseInt(getCookie('newsletter-dismissed'), 10);
    const newsletterSubscribed = parseInt(
      getCookie('newsletter-subscribed'),
      10,
    );

    if (!newsletterDismissed && !newsletterSubscribed) {
      document.addEventListener('interaction', () => {
        setTimeout(() => {
          showOverlay(overlay);
          setTimeout(() => {
            allowClose = true;
          }, 1000);
        }, 2000);
      });
    }
  });
};
