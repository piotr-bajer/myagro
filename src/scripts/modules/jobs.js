import { on } from '../helpers/shortcuts';

export default () => {
  const jobsMoreButton = document.querySelector('.c-jobs__region-button--more');

  if (jobsMoreButton !== null) {
    on('click', jobsMoreButton, event => {
      event.preventDefault();
      jobsMoreButton.closest('.c-jobs__region').classList.remove('has-more');
    });
  }
};
