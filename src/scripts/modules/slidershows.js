import { on, each, createEvent } from '../helpers/shortcuts';
import S from './shared';

export default () => {
  const slideshowContainers = document.querySelectorAll('.js-slideshow');

  if (slideshowContainers.length < 1) {
    return;
  }

  each(slideshowContainers, slideshowContainer => {
    const items = slideshowContainer.querySelector('.js-slideshow-items');

    if (items.children.length < 2) {
      return;
    }

    const autoplay = parseInt(slideshowContainer.dataset.auto, 10);
    const nav = slideshowContainer.querySelector('.js-slideshow-nav');
    const next = slideshowContainer.querySelector('.js-slideshow-next');
    const prev = slideshowContainer.querySelector('.js-slideshow-prev');
    let top = slideshowContainer.querySelector('.js-slideshow-top');
    if (top === null) {
      top = slideshowContainer;
    }
    let currentSlide = 0;
    let changeTimeout = null;
    const slideshowTime = !Number.isNaN(autoplay) ? autoplay : 4000;

    items.classList.add(S.CSS.initialized);

    const setSlide = (index, scrollTo = true) => {
      items.children[currentSlide].classList.remove(S.CSS.active);

      if (nav.children[currentSlide] !== null) {
        nav.children[currentSlide].classList.remove(S.CSS.active);
      }

      currentSlide = index;
      items.children[currentSlide].classList.add(S.CSS.active);

      if (nav.children[currentSlide] !== null) {
        nav.children[currentSlide].classList.add(S.CSS.active);
      }

      if (scrollTo) {
        const itemsY = items.getBoundingClientRect().top + S.scrollTop;
        const topY = top.getBoundingClientRect().top + S.scrollTop;

        if (itemsY < S.scrollTop + S.getHeaderHeight() + S.adminBarHeight) {
          window.scrollTo({
            top: topY - S.getHeaderHeight() - S.adminBarHeight,
            left: 0,
            behavior: 'smooth',
          });
        }
      }
    };

    if (slideshowContainer.classList.contains('js-slideshow-auto')) {
      const nextSlide = () => {
        clearTimeout(changeTimeout);

        changeTimeout = setTimeout(() => {
          setSlide((currentSlide + 1) % items.children.length, false);
          nextSlide();
        }, slideshowTime);
      };

      nextSlide();
    }

    setSlide(currentSlide, false);

    on('click', nav.children, event => {
      event.preventDefault();

      setSlide(
        [].indexOf.call(nav.children, event.currentTarget),
        event.skipAnimation !== true,
      );

      clearTimeout(changeTimeout);
    });

    if (prev !== null) {
      on('click', prev, event => {
        event.preventDefault();

        setSlide(
          currentSlide - 1 < 0 ? items.children.length - 1 : currentSlide - 1,
        );

        clearTimeout(changeTimeout);

        const slideEvent = createEvent('slide-change');
        slideEvent.slideIndex = currentSlide;
        slideEvent.slideshowContainer = slideshowContainer;
        document.dispatchEvent(slideEvent);
      });
    }

    if (next !== null) {
      on('click', next, event => {
        event.preventDefault();

        setSlide((currentSlide + 1) % items.children.length);

        clearTimeout(changeTimeout);

        const slideEvent = createEvent('slide-change');
        slideEvent.slideIndex = currentSlide;
        slideEvent.slideshowContainer = slideshowContainer;
        document.dispatchEvent(slideEvent);
      });
    }
  });
};
