import S from './shared';
import { on, each, nextFrame } from '../helpers/shortcuts';
import { lockViewport, unlockViewport } from '../helpers/viewport-lock';
import lazyload from './lazy-load';

export default () => {
  const memberContainers = document.querySelectorAll('.c-members');

  if (memberContainers.length < 1) {
    return;
  }

  const url = window.APP_DATA.ajax_url;

  each(memberContainers, memberContainer => {
    const loadedMembers = {};
    const moreButtons = memberContainer.querySelectorAll('.c-members__more');
    const textContainers = memberContainer.querySelectorAll(
      '.c-member__text-inner',
    );
    const itemsContainers = memberContainer.querySelectorAll(
      '.c-members__items',
    );
    const overlay = memberContainer.querySelector('.c-overlay');
    const overlayInner = overlay.querySelector('.c-overlay__inner');
    let currentMember = 0;
    let overlayTimeout = null;

    const showMember = id => {
      if (loadedMembers[id] !== undefined) {
        loadedMembers[id].classList.remove(S.CSS.hidden);
      }
    };

    const hideMember = id => {
      if (loadedMembers[id] !== undefined) {
        loadedMembers[id].classList.add(S.CSS.hidden);
      }
    };

    const showOverlay = () => {
      if (overlay.classList.contains(S.CSS.active)) {
        return;
      }

      overlay.classList.add(S.CSS.active);
      lockViewport(overlay);
      clearTimeout(overlayTimeout);
    };

    const hideOverlay = () => {
      if (!overlay.classList.contains(S.CSS.active)) {
        return;
      }

      overlay.classList.remove(S.CSS.active);
      unlockViewport(overlay);

      overlayTimeout = setTimeout(() => {
        hideMember(currentMember);
        currentMember = 0;
      }, 500);
    };

    const loadMember = id => {
      showOverlay();

      if (loadedMembers[id] === undefined) {
        const [realId, taxonomy] = id.split('_');
        overlay.classList.add(S.CSS.loading);
        window
          .fetch(url, {
            method: 'POST',
            body: new URLSearchParams({
              id: realId,
              taxonomy,
              action: 'member_data',
            }),
          })
          .then(response => response.json())
          .then(data => {
            if (data.success) {
              overlayInner.insertAdjacentHTML('beforeend', data.data.html);
              lazyload().update();
              const loadedMember = overlayInner.lastElementChild;
              loadedMembers[data.data.id] = loadedMember;

              if (currentMember.toString() === data.data.id.toString()) {
                nextFrame(() => {
                  loadedMember.classList.remove(S.CSS.hidden);
                  overlay.classList.remove(S.CSS.loading);
                });
              }
            }
          });
      } else {
        hideMember(currentMember);
        showMember(id);
      }

      currentMember = id;
    };

    const checkItems = () => {
      let itemsPerPage = 16;

      if (S.windowWidth < S.breakpoints.medium) {
        itemsPerPage = 4;
      } else if (S.windowWidth < S.breakpoints.large) {
        itemsPerPage = 8;
      } else if (S.windowWidth < S.breakpoints.xlarge) {
        itemsPerPage = 12;
      }

      each(itemsContainers, container => {
        if (container.children.length - 1 >= itemsPerPage) {
          container.classList.remove('no-more');
        } else {
          container.classList.add('no-more');
        }
      });

      if (S.windowWidth < S.breakpoints.medium) {
        each(textContainers, container => {
          if (
            container.parentNode.classList.contains(S.CSS.initialized) &&
            container.clientHeight <= container.scrollHeight
          ) {
            container.parentNode.classList.add(S.CSS.initialized);
          }
        });
      }
    };

    on('click', memberContainer, event => {
      const path = event.composedPath();

      for (let i = 0; i < path.length; i += 1) {
        const el = path[i];

        if (el.classList) {
          if (el.classList.contains('c-member__more')) {
            event.stopPropagation();
            el.parentNode.classList.add(S.CSS.initialized);
            break;
          } else if (el.classList.contains('c-overlay__inner')) {
            event.stopPropagation();
            break;
          } else if (el.classList.contains('c-members__item')) {
            event.stopPropagation();
            overlay.scrollTo({ top: 0 });
            loadMember(el.dataset.id);
            break;
          } else if (el.dataset && el.dataset.overlayClose !== undefined) {
            event.preventDefault();
            event.stopPropagation();
            hideOverlay();
            break;
          } else if (
            el.dataset &&
            (el.classList.contains('c-member__prev') ||
              el.classList.contains('c-member__next'))
          ) {
            const { id } = el.dataset;
            const currentMembersContainer = memberContainer.querySelector(
              '.c-members__items.is-active',
            );

            if (currentMembersContainer === null) {
              break;
            }

            const card = currentMembersContainer.querySelector(
              `[data-member-id="${id}"]`,
            );

            if (card === null) {
              break;
            }

            event.stopPropagation();
            overlay.scrollTo({ top: 0, behavior: 'smooth' });
            hideMember(currentMember);

            let newIndex;
            const items = currentMembersContainer.querySelectorAll(
              '.c-members__item',
            );
            const currentIndex = [].indexOf.call(items, card);

            if (el.classList.contains('c-member__prev')) {
              newIndex = (currentIndex > 0 ? currentIndex : items.length) - 1;
            } else {
              newIndex = (currentIndex + 1) % items.length;
            }

            loadMember(items[newIndex].dataset.id);
            break;
          }
        }
      }
    });

    on('click', moreButtons, event => {
      event.preventDefault();

      event.currentTarget.parentNode.parentNode.classList.add(
        S.CSS.initialized,
      );
    });

    on('resize', window, checkItems);

    checkItems();
  });
};
