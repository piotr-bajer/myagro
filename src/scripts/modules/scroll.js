import ScrollBooster from '../helpers/scrollbooster';
import { each } from '../helpers/shortcuts';

export default () => {
  const scrollXContainers = document.querySelectorAll('.js-scroll-x');

  if (scrollXContainers.length < 1) {
    return;
  }

  const scrollBoosters = [];

  each(scrollXContainers, container =>
    scrollBoosters.push(
      new ScrollBooster({
        viewport: container,
        content: container,
        mode: 'x',
        onUpdate: data => {
          // eslint-disable-next-line
          container.scrollLeft = data.position.x;
        },
      }),
    ),
  );
};
