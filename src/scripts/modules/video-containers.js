import { on, each } from '../helpers/shortcuts';
import S from './shared';
import loadScript from '../helpers/load-script';

export default () => {
  const videoContainers = document.querySelectorAll('.js-video-container');

  if (videoContainers.length < 1) {
    return;
  }

  window.onYouTubeIframeAPIReady = () => {
    each(videoContainers, videoContainer => {
      const embed = videoContainer.querySelector('.js-video-embed');

      if (embed.children.length < 1) {
        return;
      }

      const button = videoContainer.querySelector('.js-video-button');

      if (button === null) {
        return;
      }

      let requestPlay = false;
      let isVideoReady = false;

      const iframe = embed.firstElementChild;
      const player = new window.YT.Player(iframe, {
        events: {
          onReady: () => {
            isVideoReady = true;

            if (requestPlay) {
              player.playVideo();
            }
          },
        },
      });

      on('click', button, event => {
        event.preventDefault();
        videoContainer.classList.add(S.CSS.active);
        requestPlay = true;

        if (isVideoReady) {
          player.playVideo();
        }
      });
    });
  };

  loadScript('https://www.youtube.com/iframe_api');
};
