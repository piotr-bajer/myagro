import { on, each } from '../helpers/shortcuts';
import S from './shared';

export default () => {
  const selectInputs = document.querySelectorAll('.o-select');

  if (selectInputs.length < 1) {
    return;
  }

  each(selectInputs, selectInput => {
    const valueContainer = selectInput.querySelector('.o-select__value');
    const valueButton = valueContainer.querySelector('.o-select__option');
    const optionsContainer = selectInput.querySelector('.o-select__options');

    on('click', valueContainer, event => {
      if (S.windowWidth >= S.breakpoints.large) {
        return;
      }

      event.preventDefault();
      event.stopPropagation();

      selectInput.classList.toggle(S.CSS.active);
    });

    on('click', document.documentElement, () => {
      selectInput.classList.remove(S.CSS.active);
    });

    on('click', optionsContainer.children, event => {
      if (S.windowWidth >= S.breakpoints.large) {
        return;
      }

      event.preventDefault();

      [].forEach.call(optionsContainer.children, option => {
        option.classList.remove(S.CSS.active);
      });

      event.currentTarget.classList.add(S.CSS.active);

      valueButton.firstElementChild.textContent =
        event.currentTarget.firstElementChild.textContent;
    });
  });
};
