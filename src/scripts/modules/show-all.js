import { on } from '../helpers/shortcuts';
import S from './shared';

const iconButtons = () => {
  const showAllButtons = document.querySelectorAll('.c-icons__show-all');

  if (showAllButtons.length < 1) {
    return;
  }

  on('click', showAllButtons, event => {
    event.preventDefault();

    const icons = event.currentTarget.closest('.c-icons');

    if (icons !== undefined) {
      icons.classList.add(S.CSS.initialized);
    }
  });
};

const statsButtons = () => {
  const showAllButtons = document.querySelectorAll('.c-stats__view-all');

  if (showAllButtons.length < 1) {
    return;
  }

  on('click', showAllButtons, event => {
    event.preventDefault();
    event.currentTarget.parentNode.classList.add(S.CSS.initialized);
  });
};

export default () => {
  iconButtons();
  statsButtons();
};
