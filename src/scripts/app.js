/*
  Project: myAgro
  Author: Piotr Bajer
 */

import 'whatwg-fetch';
import 'url-search-params-polyfill';
import smoothscroll from 'smoothscroll-polyfill';
import fix100vh from './fixes/100vh';
import objectFit from './polyfills/object-fit';
import composedPath from './polyfills/composed-path';
import closest from './polyfills/closest';
import lazyLoad from './modules/lazy-load';
import header from './modules/header';
import hero from './modules/hero';
import videoContainers from './modules/video-containers';
import slideshows from './modules/slidershows';
import imageSelects from './modules/image-selects';
import selects from './modules/selects';
import showAllButtons from './modules/show-all';
import members from './modules/members';
import postsList from './modules/posts-list';
import newsletterPopup from './modules/overlay-newsletter';
import scroll from './modules/scroll';
import mailchimpForm from './modules/mailchimp-form';
import links from './modules/links';
import shareButtons from './modules/share-buttons';
import jobs from './modules/jobs';
import teamGallery from './modules/team-gallery';

smoothscroll.polyfill();
fix100vh();
objectFit();
composedPath();
closest();
lazyLoad();
header();
hero();
videoContainers();
slideshows();
imageSelects();
selects();
showAllButtons();
members();
postsList();
newsletterPopup();
scroll();
mailchimpForm();
links();
shareButtons();
jobs();
teamGallery();
