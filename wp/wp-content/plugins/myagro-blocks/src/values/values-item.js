import classnames from 'classnames';
import BackgroundSelect from '../common/background-select';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, MediaUpload, MediaUploadCheck, InspectorControls } = wp.blockEditor;
const { Fragment } = wp.element;
const { Button } = wp.components;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;
const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'myagro/values-item', {
	title: __( 'Value' ),
	parent: [ 'myagro/values' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		title: {
			type: 'string',
		},
		label: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
		icon: {
			type: 'number',
		},
	},

	edit: withSelect( ( select, props ) => {
		const { clientId } = props;
		const { getMedia } = select( 'core' );
		const { img, icon } = props.attributes;
		const { getBlockIndex, getBlockRootClientId, getBlockAttributes } = select( 'core/block-editor' );
		const rootClientId = getBlockRootClientId( clientId );
		const rootAttributes = getBlockAttributes( rootClientId );
		const blockIndex = getBlockIndex( clientId, rootClientId );
		return {
			imageObject: img ? getMedia( img ) : null,
			iconObject: icon ? getMedia( icon ) : null,
			isVisible: rootAttributes.currentSlideUid === clientId || ( blockIndex === 0 && rootAttributes.currentSlideUid === null ),
		};
	} )( ( { className, attributes, setAttributes, isSelected, imageObject, isVisible, iconObject } ) => {
		if ( ! isVisible ) {
			return null;
		}

		const classes = classnames( className, 'c-values__item' );
		const { title, text, img, icon } = attributes;
		const imgSrc = imageObject && imageObject.source_url ? imageObject.source_url : '';

		const panelControls = <InspectorControls>
			{ <BackgroundSelect imageName="Icon" instructions={ instructions } bgImage={ iconObject } img={ icon } onRemoveImage={ () =>
				setAttributes( {
					icon: undefined,
				} ) } onUpdateImage={ image =>
				setAttributes( {
					icon: image.id,
				} )
			} /> }
		</InspectorControls>;

		return ( <Fragment>
			{ panelControls }
			<div className={ classes }>
				<RichText
					className="c-values__item-title o-h5 u-fw-700 u-uppercase u-c-green"
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<div className="c-values__item-content">
					<div className="c-values__item-media">
						{ imgSrc && <img className="c-values__item-image u-fit-contain" src={ imgSrc } alt="" /> }
						<MediaUploadCheck fallback={ instructions }>
							<MediaUpload
								onSelect={ ( image ) => {
									setAttributes( {
										img: image.id,
									} );
								} }
								allowedTypes={ ALLOWED_MEDIA_TYPES }
								value={ img }
								render={ ( { open } ) => img ?
									<Fragment>
										{ isSelected && <Fragment>
											<Button isSecondary onClick={ () => {
												setAttributes( {
													img: undefined,
												} );
											} }>
												Remove Image
											</Button></Fragment> }
									</Fragment> :
									( <Button isSecondary onClick={ open }>
										Add Image
									</Button> ) }
							/>
						</MediaUploadCheck>
					</div>
					<RichText
						className="c-values__item-text o-h2 u-fw-300 u-fs-normal u-ff-serif"
						value={ text }
						onChange={ ( content ) => setAttributes( { text: content } ) }
						placeholder={ 'Enter Text' }
						withoutInteractiveFormatting
						allowedFormats={ [ ] }
					/>
				</div>
			</div>
		</Fragment>
		);
	} ),

	save: () => null,
} );
