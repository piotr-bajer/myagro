import './editor.scss';
import './style.scss';
import classnames from 'classnames';
import applySlider from '../common/slider';
import URLPicker from '../common/url-picker';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;
const { select, dispatch } = wp.data;

registerBlockType( 'myagro/values', {
	title: __( 'Values' ),
	icon: 'format-gallery',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		currentSlideUid: {
			type: 'string',
			default: null,
		},
		buttonText: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
		isOpen: {
			type: 'boolean',
			default: false,
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: applySlider( ( { className, attributes, setAttributes, blockIndex, innerBlocks, isSelected, iconControls } ) => {
		const classes = classnames( className, 'c-values o-wrapper-padding-wide o-section u-no-last-margin' );
		const { title, buttonText, url, rel, target, isOpen, currentSlideUid } = attributes;
		const options = [];
		let currentOption = null;
		let nextUid = null;
		let prevUid = null;
		const { getMedia } = select( 'core' );

		innerBlocks.forEach( ( block, index ) => {
			const { icon, label } = block.attributes;
			const imageObject = icon ? getMedia( icon ) : null;
			const imageSrc = imageObject && imageObject.media_details.sizes.circle.source_url;
			const option = <div key={ index } className={ 'o-image-select__option' + ( blockIndex === index ? ' is-active' : '' ) } onClick={ () => setAttributes( { currentSlideUid: block.clientId, isOpen: false } ) } role="button">
				<div className="o-image-select__media">
					{ imageSrc && <img className="o-image-select__image u-fit-cover" src={ imageSrc } alt="" /> }
				</div>
				<RichText
					className={ 'o-image-select__label o-h4 u-ff-serif u-fw-400' }
					value={ label }
					onChange={ ( content ) => dispatch( 'core/block-editor' ).updateBlockAttributes( block.clientId, { label: content } ) }
					placeholder={ 'Add text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
			</div>;

			if ( blockIndex === index ) {
				currentOption = <div className={ 'o-image-select__option u-hide-large' + ( blockIndex === index ? ' is-active' : '' ) } onClick={ () => setAttributes( { isOpen: ! isOpen } ) } role="button">
					<div className="o-image-select__media">
						{ imageSrc && <img className="o-image-select__image u-fit-cover" src={ imageSrc } alt="" /> }
					</div>
					<div className="o-image-select__label o-h4 u-ff-serif u-fw-400">{ label }</div>
				</div>;
				nextUid = innerBlocks[ index + 1 ] !== undefined ? innerBlocks[ index + 1 ].clientId : null;
				prevUid = innerBlocks[ index - 1 ] !== undefined ? innerBlocks[ index - 1 ].clientId : null;
			}

			options.push( option );
		} );

		return (
			<Fragment>
				{ iconControls }
				<div className={ classes }>
					<RichText
						className={ 'c-values__title o-h1 u-fw-300 u-ff-serif u-text-center u-c-green' }
						value={ title }
						onChange={ ( content ) => setAttributes( { title: content } ) }
						placeholder={ 'Add title' }
						withoutInteractiveFormatting
						allowedFormats={ [] }
					/>
					{ innerBlocks.length > 1 && <div className={ 'c-values__select o-image-select' + ( isOpen ? ' is-active' : '' ) }>
						<div className="o-image-select__value">
							{ currentOption }
						</div>
						<div className="o-image-select__options">
							{ options }
						</div>
					</div> }
					<div className="c-values__items-wrapper">
						<InnerBlocks
							className="c-values__items"
							allowedBlocks={ [ 'myagro/values-item' ] }
							template={ [ [ 'myagro/values-item' ] ] }
							__experimentalMoverDirection="horizontal"
						/>
						<div className="c-values__nav u-fw-700 o-next-prev">
							<div className="c-values__prev o-next-prev__item" onClick={ () => prevUid && setAttributes( { currentSlideUid: prevUid } ) } role="button">Previous</div>
							<div className="c-values__next o-next-prev__item o-next-prev__item--next" onClick={ () => nextUid && setAttributes( { currentSlideUid: nextUid } ) } role="button">Next</div>
						</div>
					</div>
					<div className="u-text-center">
						<RichText
							className={ 'c-values__cta o-btn o-btn--green o-btn--round o-btn--letter-spacing u-uppercase' }
							value={ buttonText }
							onChange={ ( content ) => setAttributes( { buttonText: content } ) }
							placeholder={ 'Add text' }
							withoutInteractiveFormatting
							allowedFormats={ [] }
						/>
						<URLPicker
							url={ url }
							setAttributes={ setAttributes }
							isSelected={ isSelected }
							opensInNewTab={ target === '_blank' }
							rel={ rel }
						/>
					</div>
				</div>
			</Fragment>
		);
	} ),

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
