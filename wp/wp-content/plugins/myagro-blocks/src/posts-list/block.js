//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Disabled } = wp.components;
const ServerSideRender = wp.serverSideRender;

registerBlockType( 'myagro/posts-list', {
	title: __( 'Posts List' ),
	icon: 'list-view',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		anchor: {
			type: 'string',
		},
		title: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: () => {
		return <Disabled>
			<ServerSideRender
				block="myagro/posts-list"
			/>
		</Disabled>;
	},

	save: () => {
		return null;
	},
} );
