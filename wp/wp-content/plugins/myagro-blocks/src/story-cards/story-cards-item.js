import classnames from 'classnames';
import backgroundSelect from '../common/background-select';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls } = wp.blockEditor;
const { PanelBody, TextareaControl, TextControl } = wp.components;
const { Fragment } = wp.element;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;

registerBlockType( 'myagro/story-cards-item', {
	title: __( 'Card' ),
	parent: [ 'myagro/story-cards' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		title: {
			type: 'string',
		},
		name: {
			type: 'string',
		},
		location: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},

	edit: withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;

		return {
			bgImage: img ? getMedia( img ) : null,
		};
	} )( ( { className, attributes, setAttributes, isSelected, bgImage } ) => {
		const classes = classnames( className, 'c-story-cards__item' );
		const { title, text, img, url, target, rel, location, name } = attributes;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';

		const onUpdateImage = ( image ) => {
			setAttributes( {
				img: image.id,
			} );
		};

		const onRemoveImage = () => {
			setAttributes( {
				img: undefined,
			} );
		};

		const bgSelectComponent = backgroundSelect( { instructions, bgImage, img, onRemoveImage, onUpdateImage } );

		const panelControls = <InspectorControls>
			{ bgSelectComponent }
			<PanelBody title="Backside text" initialOpen={ true }>
				<TextControl
					label={ __( 'Name' ) }
					value={ name }
					onChange={ ( content ) => setAttributes( { name: content } ) }
				/>
				<TextControl
					label={ __( 'Location' ) }
					value={ location }
					onChange={ ( content ) => setAttributes( { location: content } ) }
				/>
				<TextareaControl
					label="Description"
					help="The text will be shown when tile is flipped"
					value={ text }
					onChange={ ( content ) => setAttributes( { text: content } ) }
				/>
			</PanelBody>
		</InspectorControls>;

		return (
			<Fragment>
				{ panelControls }
				<div className={ classes }>
					{ bgSrc && <img className="c-story-cards__item-bg u-fit-cover" src={ bgSrc } alt="" /> }
					<RichText
						className="c-story-cards__item-title u-fw-500 o-h4 u-ff-serif u-text-center"
						value={ title }
						onChange={ ( content ) => setAttributes( { title: content } ) }
						placeholder={ 'Enter Card Title' }
						withoutInteractiveFormatting
						allowedFormats={ [] }
					/>
					<URLPicker
						url={ url }
						setAttributes={ setAttributes }
						isSelected={ isSelected }
						opensInNewTab={ target === '_blank' }
						rel={ rel }
					/>
				</div>
			</Fragment>
		);
	} ),

	save: () => null,
} );
