import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/story-cards', {
	title: __( 'Story Cards' ),
	icon: 'grid-view',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { className, attributes, setAttributes } ) => {
		const classes = classnames( className, 'c-story-cards o-section' );
		const { title } = attributes;

		return (
			<div className={ classes }>
				<RichText
					className={ 'c-story-cards__title o-h1 u-c-green u-fw-300 u-ff-serif u-text-center' }
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Add title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<InnerBlocks
					className="c-story-cards__items"
					allowedBlocks={ [ 'myagro/story-cards-item' ] }
					template={ [ [ 'myagro/story-cards-item' ] ] }
					__experimentalMoverDirection="horizontal"
				/>
			</div>
		);
	},

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
