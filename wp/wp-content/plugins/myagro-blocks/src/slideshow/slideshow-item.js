import classnames from 'classnames';
import { isVisible as applyIsVisible } from '../common/slider';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { MediaUpload, MediaUploadCheck, InspectorControls } = wp.blockEditor;
const { Fragment } = wp.element;
const { withSelect } = wp.data;
const { PanelBody, Button, TextControl } = wp.components;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;
const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'myagro/slideshow-item', {
	title: __( 'Slide' ),
	parent: [ 'myagro/slideshow' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		img: {
			type: 'number',
		},
		url: {
			type: 'string',
		},
		isVisible: {
			type: 'boolean',
			default: false,
		},
	},

	edit: applyIsVisible( withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;
		return {
			bgImage: img ? getMedia( img ) : null,
		};
	} )( ( { className, attributes, setAttributes, isSelected, bgImage, isVisible } ) => {
		if ( ! isVisible ) {
			return null;
		}

		const classes = classnames( className, 'c-slideshow__item' );
		const { img, url } = attributes;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';

		const panelControls = <InspectorControls>
			<PanelBody
				title="Video URL"
				initialOpen={ true }
			>
				<div>
					<TextControl
						label="Video URL"
						value={ url }
						onChange={ ( newURL ) => setAttributes( { url: newURL } ) }
					/>
				</div>
			</PanelBody>
		</InspectorControls>;

		return <Fragment>
			{ panelControls }
			<div className={ classes }>
				<div className="c-slideshow__media js-video-container">
					{ bgSrc && <img className="c-slideshow__image u-fit-cover js-video-cover" src={ bgSrc } alt="" /> }
					<MediaUploadCheck fallback={ instructions }>
						<MediaUpload
							onSelect={ ( image ) => {
								setAttributes( {
									img: image.id,
								} );
							} }
							allowedTypes={ ALLOWED_MEDIA_TYPES }
							value={ img }
							render={ ( { open } ) => img ?
								<Fragment>
									{ isSelected && <Fragment>
										<Button isSecondary onClick={ () => {
											setAttributes( {
												img: undefined,
											} );
										} }>
											Remove Image
										</Button></Fragment> }
								</Fragment> :
								( <Button isSecondary onClick={ open }>
									Add Image
								</Button> ) }
						/>
					</MediaUploadCheck>
					{ bgSrc && <button className="c-slideshow__button o-play-button js-video-button" type="button">
						Play
					</button> }
				</div>
			</div>
		</Fragment>;
	} ) ),

	save: () => null,
} );
