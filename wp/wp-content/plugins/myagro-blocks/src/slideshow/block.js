import './editor.scss';
import './style.scss';
import classnames from 'classnames';
import URLPicker from '../common/url-picker';
import applySlider from '../common/slider';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/slideshow', {
	title: __( 'Slideshow' ),
	icon: 'slides',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		currentSlideUid: {
			type: 'string',
			default: null,
		},
		buttonText: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: applySlider( ( { className, attributes, setAttributes, blockIndex, blockUids, isSelected } ) => {
		const classes = classnames( className, 'c-slideshow o-wrapper-padding o-section u-no-last-margin u-text-center' );
		const { title, buttonText, rel, target, url } = attributes;
		const dots = blockUids.map( ( uid, index ) => <div key={ index } className={ 'o-dots__button' + ( blockIndex === index ? ' is-active' : '' ) } onClick={ () => setAttributes( { currentSlideUid: uid } ) } role="button">
			<span className="o-dots__dot"><span className="u-hidden-visually">{ index + 1 }</span></span>
		</div> );

		return (
			<div className={ classes }>
				<RichText
					className={ 'c-slideshow__title o-h1 u-fw-300 u-ff-serif u-text-center u-c-green' }
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Add title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				{ <InnerBlocks
					className="c-slideshow__items"
					allowedBlocks={ [ 'myagro/slideshow-item' ] }
					template={ [ [ 'myagro/slideshow-item' ] ] }
					__experimentalMoverDirection="horizontal"
				/> }
				{ blockUids.length > 1 && <div className="c-slideshow__dots o-dots o-dots--green">
					{ dots }
				</div>
				}
				<RichText
					className={ 'c-slideshow__cta o-btn o-btn--green o-btn--round o-btn--letter-spacing u-uppercase' }
					value={ buttonText }
					onChange={ ( content ) => setAttributes( { buttonText: content } ) }
					placeholder={ 'Add text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<URLPicker
					url={ url }
					setAttributes={ setAttributes }
					isSelected={ isSelected }
					opensInNewTab={ target === '_blank' }
					rel={ rel }
				/>
			</div>
		);
	} ),

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
