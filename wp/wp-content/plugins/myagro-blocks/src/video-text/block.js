import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { withSelect } = wp.data;
const { compose } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls, MediaUpload, MediaUploadCheck, InnerBlocks } = wp.blockEditor;
const { PanelBody, Button, TextControl } = wp.components;

const ALLOWED_MEDIA_TYPES = [ 'image' ];
const instructions = <p>{ __( 'To edit the video cover image, you need permission to upload media.' ) }</p>;

registerBlockType( 'myagro/video-text', {
	title: __( 'Video & Text' ),
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		align: {
			type: 'string',
			default: 'wide',
		},
		url: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
		anchor: {
			type: 'string',
		},
	},
	supports: {
		html: false,
		anchor: true,
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'wide', align: 'wide' };
	},

	edit: compose(
		withSelect( ( select, props ) => {
			const { getMedia } = select( 'core' );
			const { img } = props.attributes;

			return {
				imgObject: img ? getMedia( img ) : null,
			};
		} ),
	)( ( { attributes, setAttributes, imgObject, className, isSelected } ) => {
		const classes = classnames( className, 'c-video-text o-section' );
		const imgSrc = imgObject && imgObject.source_url ? imgObject.source_url : '';
		const { img, url } = attributes;

		const panelControls = <InspectorControls>
			<PanelBody
				title="Video URL"
				initialOpen={ true }
			>
				<div>
					<TextControl
						label="Video URL"
						value={ url }
						onChange={ ( newURL ) => setAttributes( { url: newURL } ) }
					/>
				</div>
			</PanelBody>
		</InspectorControls>;

		return (
			<Fragment>
				{ panelControls }
				<div className={ classes }>
					<InnerBlocks
						className="c-video-text__text u-no-last-margin"
						allowedBlocks={ [ 'core/paragraph', 'core/headline', 'core/list', 'core/image', 'core/buttons' ] }
						template={ [ [ 'core/heading' ], [ 'core/paragraph' ] ] }
					/>
					<div className="c-video-text__media js-video-container">
						{ imgSrc && <img className="c-video-text__cover u-fit-cover js-video-cover" src={ imgSrc } alt="" /> }
						<MediaUploadCheck fallback={ instructions }>
							<MediaUpload
								onSelect={ ( image ) => {
									setAttributes( {
										img: image.id,
									} );
								} }
								allowedTypes={ ALLOWED_MEDIA_TYPES }
								value={ img }
								render={ ( { open } ) => img ?
									<Fragment>
										{ isSelected && <Fragment>
											<Button isSecondary onClick={ () => {
												setAttributes( {
													img: undefined,
												} );
											} }>
												Remove Cover Image
											</Button></Fragment> }
									</Fragment> :
									( <Button isSecondary onClick={ open }>
										Add Cover Image
									</Button> ) }
							/>
						</MediaUploadCheck>
						{ imgSrc && <button className="c-video-text__button o-play-button js-video-button" type="button">
							Play
						</button> }
					</div>
				</div>
			</Fragment>
		);
	} ),

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
