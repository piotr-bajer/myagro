import './editor.scss';
import './style.scss';
import classnames from 'classnames';
import applySlider from '../common/slider';

const { InnerBlocks, RichText, InspectorControls } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;
const { select, dispatch } = wp.data;
const { PanelBody, TextControl } = wp.components;

registerBlockType( 'myagro/team-gallery', {
	title: __( 'Team Gallery' ),
	icon: 'format-gallery',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		currentSlideUid: {
			type: 'string',
			default: null,
		},
		isOpen: {
			type: 'boolean',
			default: false,
		},
		autoplay: {
			type: 'number',
			default: 0,
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: applySlider( ( { className, attributes, setAttributes, blockIndex, innerBlocks, iconControls } ) => {
		const classes = classnames( className, 'c-team-gallery o-wrapper-padding-wide o-section u-no-last-margin' );
		const { isOpen, autoplay } = attributes;
		const options = [];
		let currentOption = null;
		const { getMedia } = select( 'core' );

		innerBlocks.forEach( ( block, index ) => {
			const { icon, label } = block.attributes;
			const imageObject = icon ? getMedia( icon ) : null;
			const imageSrc = imageObject && imageObject.media_details.sizes.circle.source_url;
			const option = <div key={ index } className={ 'o-image-select__option' + ( blockIndex === index ? ' is-active' : '' ) } onClick={ () => setAttributes( { currentSlideUid: block.clientId, isOpen: false } ) } role="button">
				<div className="o-image-select__media">
					{ imageSrc && <img className="o-image-select__image u-fit-contain" src={ imageSrc } alt="" /> }
				</div>
				<RichText
					className={ 'o-image-select__label o-h4 u-ff-serif u-fw-400' }
					value={ label }
					onChange={ ( content ) => dispatch( 'core/block-editor' ).updateBlockAttributes( block.clientId, { label: content } ) }
					placeholder={ 'Add text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
			</div>;

			if ( blockIndex === index ) {
				currentOption = <div className={ 'o-image-select__option u-hide-large' + ( blockIndex === index ? ' is-active' : '' ) } onClick={ () => setAttributes( { isOpen: ! isOpen } ) } role="button">
					<div className="o-image-select__media">
						{ imageSrc && <img className="o-image-select__image u-fit-contain" src={ imageSrc } alt="" /> }
					</div>
					<div className="o-image-select__label o-h4 u-ff-serif u-fw-400">{ label }</div>
				</div>;
			}

			options.push( option );
		} );

		const panelControls = <Fragment>
			<InspectorControls>
				<PanelBody title={ __( 'Autoplay settings' ) }>
					<TextControl
						label={ __( 'Autoplay time (0 = off, 1000 = 1 second)' ) }
						value={ autoplay }
						onChange={ value => setAttributes( { autoplay: value } ) }
					/>
				</PanelBody>
			</InspectorControls>
			{ iconControls }
		</Fragment>;

		return (
			<Fragment>
				{ panelControls }
				<div className={ classes }>
					{ innerBlocks.length > 1 && <div className={ 'c-team-gallery__select o-image-select' + ( isOpen ? ' is-active' : '' ) }>
						<div className="o-image-select__value">
							{ currentOption }
						</div>
						<div className="o-image-select__options">
							{ options }
						</div>
					</div> }
					<InnerBlocks
						className="c-team-gallery__items"
						allowedBlocks={ [ 'myagro/team-gallery-item' ] }
						template={ [ [ 'myagro/team-gallery-item' ] ] }
						__experimentalMoverDirection="horizontal"
					/>
				</div>
			</Fragment>
		);
	} ),

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
