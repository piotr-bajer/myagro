import classnames from 'classnames';
import BackgroundSelect from '../common/background-select';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InspectorControls, MediaPlaceholder } = wp.blockEditor;
const { Fragment } = wp.element;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;
const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'myagro/team-gallery-item', {
	title: __( 'Gallery Slide' ),
	parent: [ 'myagro/team-gallery' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		label: {
			type: 'string',
		},
		icon: {
			type: 'number',
		},
		currentImg: {
			type: 'number',
			default: null,
		},
		imageIds: {
			items: {
				type: 'number',
			},
			default: [],
		},
	},

	edit: withSelect( ( select, props ) => {
		const { clientId } = props;
		const { getMedia } = select( 'core' );
		const { icon, currentImg, imageIds } = props.attributes;
		const { getBlockIndex, getBlockRootClientId, getBlockAttributes } = select( 'core/block-editor' );
		const rootClientId = getBlockRootClientId( clientId );
		const rootAttributes = getBlockAttributes( rootClientId );
		const blockIndex = getBlockIndex( clientId, rootClientId );
		return {
			iconObject: icon ? getMedia( icon ) : null,
			currentImgObject: imageIds && imageIds[ currentImg ] ? getMedia( imageIds[ currentImg ] ) : null,
			isVisible: rootAttributes.currentSlideUid === clientId || ( blockIndex === 0 && rootAttributes.currentSlideUid === null ),
		};
	} )( ( { className, attributes, setAttributes, isVisible, iconObject, currentImgObject, noticeOperations, noticeUI, onFocus } ) => {
		if ( ! isVisible ) {
			return null;
		}

		const classes = classnames( className, 'c-team-gallery__item' );
		const { icon, currentImg, imageIds } = attributes;
		const currentImgSrc = currentImgObject && currentImgObject.source_url ? currentImgObject.source_url : '';
		const dots = imageIds.map( ( id, index ) => <div key={ index } className={ 'o-dots__button' + ( currentImg === index ? ' is-active' : '' ) } onClick={ () => setAttributes( { currentImg: index } ) } role="button">
			<span className="o-dots__dot"><span className="u-hidden-visually">{ index + 1 }</span></span>
		</div> );

		const panelControls = <InspectorControls>
			{ <BackgroundSelect imageName="Icon" instructions={ instructions } bgImage={ iconObject } img={ icon } onRemoveImage={ () =>
				setAttributes( {
					icon: undefined,
				} ) } onUpdateImage={ image =>
				setAttributes( {
					icon: image.id,
				} )
			} /> }
		</InspectorControls>;

		const hasImages = !! imageIds.length;

		const onSelectImages = ( newImages ) => {
			setAttributes( { currentImg: 0, imageIds: newImages.map( ( newImage ) => newImage.id ) } );
		};

		const onUploadError = ( message ) => {
			noticeOperations.removeAllNotices();
			noticeOperations.createErrorNotice( message );
		};

		const imagesObject = imageIds && imageIds.length > 0 ? imageIds.map( id => ( {
			id,
		} ) ) : [];

		const mediaPlaceholder = (
			<MediaPlaceholder
				addToGallery={ hasImages }
				isAppender={ hasImages }
				className="c-team-gallery__media-placeholder"
				disableMediaButtons={ false }
				icon={ ! hasImages && 'format-gallery' }
				labels={ {
					title: ! hasImages && __( 'Gallery' ),
					instructions: ! hasImages && __( 'Drag images, upload new ones or select files from your library.' ),
				} }
				onSelect={ onSelectImages }
				accept="image/*"
				allowedTypes={ ALLOWED_MEDIA_TYPES }
				multiple
				value={ imagesObject.length > 0 ? imagesObject : {} }
				onError={ onUploadError }
				notices={ hasImages ? undefined : noticeUI }
				onFocus={ onFocus }
			/>
		);

		return ( <Fragment>
			{ panelControls }
			<div className={ classes }>
				<div className="c-team-gallery__images">
					<div className="c-team-gallery__media is-active">
						{ currentImgSrc && <img className="c-team-gallery__item-image u-fit-cover" src={ currentImgSrc } alt="" /> }
					</div>
				</div>
				{ imageIds.length > 1 && <div className="c-team-gallery__dots o-dots">
					{ dots }
				</div> }
				{ mediaPlaceholder }
			</div>
		</Fragment>
		);
	} ),

	save: () => null,
} );
