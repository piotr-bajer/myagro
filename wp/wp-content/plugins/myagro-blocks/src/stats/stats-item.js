import classnames from 'classnames';
import BackgroundSelect from '../common/background-select';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls, InnerBlocks } = wp.blockEditor;
const { Fragment } = wp.element;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;

registerBlockType( 'myagro/stats-item', {
	title: __( 'Stats' ),
	parent: [ 'myagro/stats' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		title: {
			type: 'string',
		},
		label: {
			type: 'string',
		},
		icon: {
			type: 'number',
		},
	},

	edit: withSelect( ( select, props ) => {
		const { clientId } = props;
		const { getMedia } = select( 'core' );
		const { icon } = props.attributes;
		const { getBlockIndex, getBlockRootClientId, getBlockAttributes } = select( 'core/block-editor' );
		const rootClientId = getBlockRootClientId( clientId );
		const rootAttributes = getBlockAttributes( rootClientId );
		const blockIndex = getBlockIndex( clientId, rootClientId );
		return {
			iconObject: icon ? getMedia( icon ) : null,
			isVisible: rootAttributes.currentSlideUid === clientId || ( blockIndex === 0 && rootAttributes.currentSlideUid === null ),
		};
	} )( ( { className, attributes, setAttributes, isVisible, iconObject } ) => {
		if ( ! isVisible ) {
			return null;
		}

		const classes = classnames( className, 'c-stats__item' );
		const { title, icon } = attributes;

		const panelControls = <InspectorControls>
			{ <BackgroundSelect imageName="Icon" instructions={ instructions } bgImage={ iconObject } img={ icon } onRemoveImage={ () =>
				setAttributes( {
					icon: undefined,
				} ) } onUpdateImage={ image =>
				setAttributes( {
					icon: image.id,
				} )
			} /> }
		</InspectorControls>;

		return ( <Fragment>
			{ panelControls }
			<div className={ classes }>
				<RichText
					className="c-stats__item-title o-h5 u-fw-700 u-uppercase"
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<InnerBlocks
					className="c-stats__item-stats"
					allowedBlocks={ [ 'myagro/stats-stat' ] }
					template={ [ [ 'myagro/stats-stat' ] ] }
					__experimentalMoverDirection="horizontal"
				/>
			</div>
		</Fragment>
		);
	} ),

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
