import classnames from 'classnames';
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls } = wp.blockEditor;
const { Fragment } = wp.element;
const { PanelBody, TextareaControl } = wp.components;

registerBlockType( 'myagro/stats-stat', {
	title: __( 'Stat' ),
	parent: [ 'myagro/stats-item' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		stat: {
			type: 'string',
		},
		name: {
			type: 'string',
		},
		strapline: {
			type: 'string',
		},
		description: {
			type: 'string',
		},
	},

	edit: ( { className, attributes, setAttributes } ) => {
		const classes = classnames( className, 'c-stats__stat' );
		const { stat, strapline, name, description } = attributes;

		let helpText = 'The text will be shown when tile is flipped. Text cannot exceed 220 characters.';

		helpText += ' Currently using ' + ( description && description.length ? description.length : 0 ) + '/220 chars.';

		const panelControls = <InspectorControls>
			<PanelBody title="Description" initialOpen={ true }>
				<TextareaControl
					label="Description"
					help={ helpText }
					rows="8"
					placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla."
					value={ description }
					maxlength="220"
					onChange={ ( content ) => setAttributes( { description: content } ) }
				/>
			</PanelBody>
		</InspectorControls>;

		return ( <Fragment>
			{ panelControls }
			<div className={ classes }>
				<div className="c-stats__stat-front u-text-center">
					<div className="c-stats__stat-stat">
						<RichText
							className="c-stats__stat-strapline u-c-white u-ff-serif"
							value={ strapline }
							onChange={ ( content ) => setAttributes( { strapline: content } ) }
							placeholder={ 'Strapline' }
							withoutInteractiveFormatting
							allowedFormats={ [ 'core/align' ] }
						/>
						<RichText
							className="c-stats__stat-value u-c-yellow u-ff-serif u-fw-300"
							value={ stat }
							onChange={ ( content ) => setAttributes( { stat: content } ) }
							placeholder={ 'Stat' }
							withoutInteractiveFormatting
							allowedFormats={ [ ] }
						/>
					</div>
					<RichText
						className="c-stats__stat-text u-c-white u-ff-serif"
						value={ name }
						onChange={ ( content ) => setAttributes( { name: content } ) }
						placeholder={ 'Add Description' }
						withoutInteractiveFormatting
						allowedFormats={ [] }
					/>
				</div>
			</div>
		</Fragment>
		);
	},

	save: ( ) => {
		return null;
	},
} );
