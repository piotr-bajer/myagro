import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.blockEditor;

registerBlockType( 'myagro/jobs-item', {
	title: __( 'Job' ),
	parent: [ 'myagro/jobs-region' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		title: {
			type: 'string',
		},
		type: {
			type: 'string',
			default: 'Full Time',
		},
		location: {
			type: 'string',
			default: 'On Site',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},

	edit: ( { className, attributes, setAttributes, isSelected } ) => {
		const classes = classnames( className, 'c-jobs__item' );
		const { title, type, location, url, target, rel } = attributes;

		return ( <div className={ classes }>
			<RichText
				className="c-jobs__item-title o-h3 u-fw-400 u-ff-serif"
				value={ title }
				onChange={ ( content ) => setAttributes( { title: content } ) }
				placeholder={ 'Job title' }
				withoutInteractiveFormatting
				allowedFormats={ [] }
			/>
			<RichText
				className="c-jobs__item-type"
				value={ type }
				onChange={ ( content ) => setAttributes( { type: content } ) }
				placeholder={ 'Enter Job Type' }
				withoutInteractiveFormatting
				allowedFormats={ [] }
			/>
			<RichText
				className="c-jobs__item-location u-fs-italic u-ff-serif"
				value={ location }
				onChange={ ( content ) => setAttributes( { location: content } ) }
				placeholder={ 'Enter Job Location' }
				withoutInteractiveFormatting
				allowedFormats={ [] }
			/>
			<URLPicker
				url={ url }
				setAttributes={ setAttributes }
				isSelected={ isSelected }
				opensInNewTab={ target === '_blank' }
				rel={ rel }
			/>
		</div>
		);
	},

	save: () => null,
} );
