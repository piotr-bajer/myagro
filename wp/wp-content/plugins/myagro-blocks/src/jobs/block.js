import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks, RichText, InspectorControls } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;
const { PanelBody, SelectControl, Disabled } = wp.components;
const ServerSideRender = wp.serverSideRender;

registerBlockType( 'myagro/jobs', {
	title: __( 'Jobs' ),
	icon: 'list',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		type: {
			type: 'string',
			default: 'manual',
		},
	},

	getEditWrapperProps() {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { className, attributes, setAttributes } ) => {
		const classes = classnames( className, 'c-jobs o-wrapper-padding-wide o-section u-no-last-margin' );
		const { title, type } = attributes;

		const panel = <InspectorControls>
			<PanelBody title={ __( 'Link settings' ) }>
				<SelectControl
					label="Select data source type"
					value={ type }
					options={ [
						{ label: 'Enter data manually', value: 'manual' },
						{ label: 'Download data from Recruiterbox', value: 'auto' },
					] }
					onChange={ value => {
						setAttributes( { type: value } );
					} }
				/>
			</PanelBody>
		</InspectorControls>;

		return (
			<Fragment>
				{ panel }
				<div className={ classes }>
					<RichText
						className={ 'c-jobs__title o-h1 u-fw-300 u-ff-serif u-text-center u-c-green' }
						value={ title }
						onChange={ ( content ) => setAttributes( { title: content } ) }
						placeholder={ 'Add title' }
						withoutInteractiveFormatting
						allowedFormats={ [] }
					/>
					{ type !== 'auto' ? <InnerBlocks
						className="c-jobs__regions"
						allowedBlocks={ [ 'myagro/jobs-region' ] }
						template={ [ [ 'myagro/jobs-region' ] ] }
					/> : <Disabled>
						<ServerSideRender
							block="myagro/recruiterbox"
							attributes={ { title } }
						/>
					</Disabled> }
				</div>
			</Fragment>
		);
	},

	save: () => {
		return <InnerBlocks.Content />;
	},
} );
