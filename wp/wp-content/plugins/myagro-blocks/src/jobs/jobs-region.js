import './editor.scss';
import './style.scss';
import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/jobs-region', {
	title: __( 'Region' ),
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: false,
	},
	attributes: {
		title: {
			type: 'string',
		},
		buttonText: {
			type: 'string',
			default: 'See All',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},

	edit: ( { className, attributes, setAttributes, isSelected } ) => {
		const classes = classnames( className, 'c-jobs__region' );
		const { title, buttonText, url, rel, target } = attributes;

		return (
			<div className={ classes }>
				<div className="c-jobs__region-header">
					<RichText
						className={ 'c-jobs__region-title u-uppercase u-fw-700' }
						value={ title }
						onChange={ ( content ) => setAttributes( { title: content } ) }
						placeholder={ 'Add title' }
						withoutInteractiveFormatting
						allowedFormats={ [] }
					/>
					<RichText
						className={ 'c-jobs__region-button u-c-white u-ff-serif u-fs-italic' }
						value={ buttonText }
						onChange={ ( content ) => setAttributes( { buttonText: content } ) }
						placeholder={ 'Add text' }
						withoutInteractiveFormatting
						allowedFormats={ [] }
					/>
					<URLPicker
						url={ url }
						setAttributes={ setAttributes }
						isSelected={ isSelected }
						opensInNewTab={ target === '_blank' }
						rel={ rel }
					/>
				</div>
				<InnerBlocks
					className="c-jobs__items"
					allowedBlocks={ [ 'myagro/jobs-item' ] }
					template={ [ [ 'myagro/jobs-item' ] ] }
				/>
			</div>
		);
	},

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
