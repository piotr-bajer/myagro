import './editor.scss';
import './style.scss';
import edit from './edit';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/hero', {
	title: __( 'Hero Section' ),
	icon: 'format-image',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
		anchor: {
			type: 'string',
		},
		texts: [],
		textColor: {
			type: 'string',
		},
		customTextColor: {
			type: 'string',
		},
	},
	supports: {
		html: false,
		anchor: true,
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: edit,

	save: ( ) => {
		return null;
	},
} );
