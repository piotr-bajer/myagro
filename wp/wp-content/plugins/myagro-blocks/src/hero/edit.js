import classnames from 'classnames';
import backgroundSelect from '../common/background-select';

const { __ } = wp.i18n;
const { Component, Fragment } = wp.element;
const { InspectorControls, RichText, withColors, PanelColorSettings } = wp.blockEditor;
const { PanelBody, FormTokenField } = wp.components;
const { compose } = wp.compose;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the background image, you need permission to upload media.' ) }</p>;

class HeroEdit extends Component {
	render() {
		const { attributes, setAttributes, bgImage, className, textColor, setTextColor } = this.props;
		const { img, texts, title, customTextColor } = attributes;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';
		const classes = classnames( className, 'c-hero o-wrapper-padding u-c-white' );

		const onUpdateImage = ( image ) => {
			setAttributes( {
				img: image.id,
			} );
		};

		const onRemoveImage = () => {
			setAttributes( {
				img: undefined,
			} );
		};

		const text = texts && texts[ 0 ];
		const bgSelectComponent = backgroundSelect( { instructions, bgImage, img, onRemoveImage, onUpdateImage } );

		const panelControls = <InspectorControls>
			{ bgSelectComponent }
			<PanelBody title="Dynamic lines" initialOpen={ true }>
				<FormTokenField placeholder="Enter dynamic lines" value={ texts } onChange={ values => setAttributes( { texts: values } ) } />
			</PanelBody>
			<PanelColorSettings
				title={ __( 'Color settings' ) }
				colorSettings={ [
					{
						value: textColor.color,
						onChange: setTextColor,
						label: __( 'Text color' ),
					},
				] }
			/>
		</InspectorControls>;

		return (
			<Fragment>
				{ panelControls }
				<div className={ classes }>
					{ bgSrc && <img className="c-hero__image u-fit-cover" src={ bgSrc } /> }
					<div className="c-hero__inner u-no-last-margin">
						<h1 className={ classnames( 'c-hero__title o-h1 u-fw-500 u-ff-serif', textColor && textColor.class ) } style={ customTextColor ? { color: customTextColor } : {} }>
							<RichText
								tagName="div"
								value={ title }
								onChange={ ( newTitle ) => setAttributes( { title: newTitle } ) }
								placeholder={ 'Enter block title' }
								withoutInteractiveFormatting
								allowedFormats={ [] }
							/>
							{ text && <div className="c-hero__underlined">
								<div><span>{ text }</span></div>
							</div> }
						</h1>

					</div>
				</div>
			</Fragment>
		);
	}
}

export default compose(
	withColors( { textColor: 'color' } ),
	withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;

		return {
			bgImage: img ? getMedia( img ) : null,
		};
	} ),
)( HeroEdit );
