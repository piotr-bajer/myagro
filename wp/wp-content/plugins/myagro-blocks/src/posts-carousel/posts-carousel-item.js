import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, MediaUpload, MediaUploadCheck } = wp.blockEditor;
const { Fragment } = wp.element;
const { Button } = wp.components;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;
const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'myagro/posts-carousel-item', {
	title: __( 'Tile' ),
	parent: [ 'myagro/posts-carousel' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		title: {
			type: 'string',
		},
		category: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
		buttonText: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},

	edit: withSelect( ( select, props ) => {
		const { clientId } = props;
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;
		const { getBlockIndex, getBlockRootClientId, getBlockAttributes } = select( 'core/block-editor' );
		const rootClientId = getBlockRootClientId( clientId );
		const rootAttributes = getBlockAttributes( rootClientId );
		const blockIndex = getBlockIndex( clientId, rootClientId );
		return {
			bgImage: img ? getMedia( img ) : null,
			isVisible: rootAttributes.currentSlideUid === clientId || ( blockIndex === 0 && rootAttributes.currentSlideUid === null ),
		};
	} )( ( { className, attributes, setAttributes, isSelected, bgImage, isVisible } ) => {
		if ( ! isVisible ) {
			return null;
		}

		const classes = classnames( className, 'c-posts-carousel__item' );
		const { title, text, img, category, buttonText, url, target, rel } = attributes;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';

		return ( <div className={ classes }>
			<div className="c-posts-carousel__media">
				{ bgSrc && <img className="c-posts-carousel__image u-fit-cover" src={ bgSrc } alt="" /> }
				<MediaUploadCheck fallback={ instructions }>
					<MediaUpload
						onSelect={ ( image ) => {
							setAttributes( {
								img: image.id,
							} );
						} }
						allowedTypes={ ALLOWED_MEDIA_TYPES }
						value={ img }
						render={ ( { open } ) => img ?
							<Fragment>
								{ isSelected && <Fragment>
									<Button isSecondary onClick={ () => {
										setAttributes( {
											img: undefined,
										} );
									} }>
										Remove Image
									</Button></Fragment> }
							</Fragment> :
							( <Button isSecondary onClick={ open }>
								Add Image
							</Button> ) }
					/>
				</MediaUploadCheck>
			</div>
			<div className="c-posts-carousel__content">
				<RichText
					className="c-posts-carousel__category"
					value={ category }
					onChange={ ( content ) => setAttributes( { category: content } ) }
					placeholder={ 'Category' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className="c-posts-carousel__item-title u-fw-500 o-h2 u-ff-serif u-c-green"
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Enter Tile Title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className="c-posts-carousel__text o-h6"
					value={ text }
					onChange={ ( content ) => setAttributes( { text: content } ) }
					placeholder={ 'Enter Tile Text' }
					withoutInteractiveFormatting
					allowedFormats={ [ 'core/bold' ] }
				/>
				<RichText
					className={ 'c-posts-carousel__cta u-fw-700' }
					value={ buttonText }
					onChange={ ( content ) => setAttributes( { buttonText: content } ) }
					placeholder={ 'Add text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<URLPicker
					url={ url }
					setAttributes={ setAttributes }
					isSelected={ isSelected }
					opensInNewTab={ target === '_blank' }
					rel={ rel }
				/>
			</div>
		</div>
		);
	} ),

	save: () => null,
} );
