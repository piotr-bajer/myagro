import './editor.scss';
import './style.scss';
import classnames from 'classnames';
import applySlider from '../common/slider';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/posts-carousel', {
	title: __( 'Post Carousel' ),
	icon: 'format-gallery',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		currentSlideUid: {
			type: 'string',
			default: null,
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: applySlider( ( { className, attributes, setAttributes, blockIndex, blockUids } ) => {
		const classes = classnames( className, 'c-posts-carousel o-wrapper-padding-wide o-section has-beige-lighter-background-color u-no-last-margin' );
		const { title } = attributes;
		const dots = blockUids.map( ( uid, index ) => <div key={ index } className={ 'o-dots__button' + ( blockIndex === index ? ' is-active' : '' ) } onClick={ () => setAttributes( { currentSlideUid: uid } ) } role="button">
			<span className="o-dots__dot"><span className="u-hidden-visually">{ index + 1 }</span></span>
		</div> );

		return (
			<div className={ classes }>
				<RichText
					className={ 'c-posts-carousel__title o-h1 u-fw-300 u-ff-serif u-text-center' }
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Add title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				{ <InnerBlocks
					className="c-posts-carousel__items"
					allowedBlocks={ [ 'myagro/posts-carousel-item' ] }
					template={ [ [ 'myagro/posts-carousel-item' ] ] }
					__experimentalMoverDirection="horizontal"
				/> }
				{ blockUids.length > 1 && <div className="c-posts-carousel__dots o-dots">
					{ dots }
				</div>
				}
			</div>
		);
	} ),

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
