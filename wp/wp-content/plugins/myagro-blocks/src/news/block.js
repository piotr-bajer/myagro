import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/news', {
	title: __( 'News Quotes' ),
	// icon: 'grid-view',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { className, attributes, setAttributes } ) => {
		const classes = classnames( className, 'c-news o-wrapper-padding-wide u-text-center o-section' );
		const { title } = attributes;

		return (
			<div className={ classes }>
				<RichText
					className={ 'c-news__title o-h1 u-c-green u-fw-300 u-ff-serif' }
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Add title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<InnerBlocks
					className="c-news__items"
					allowedBlocks={ [ 'myagro/news-item' ] }
					template={ [ [ 'myagro/news-item' ] ] }
					__experimentalMoverDirection="horizontal"
				/>
			</div>
		);
	},

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
