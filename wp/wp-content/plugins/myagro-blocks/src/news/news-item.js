import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, MediaUpload, MediaUploadCheck } = wp.blockEditor;
const { Fragment } = wp.element;
const { Button } = wp.components;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;
const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'myagro/news-item', {
	title: __( 'Quote' ),
	parent: [ 'myagro/news' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		quote: {
			type: 'string',
		},
		author: {
			type: 'string',
		},
		source: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
		text: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},

	edit: withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;

		return {
			bgImage: img ? getMedia( img ) : null,
		};
	} )( ( { className, attributes, setAttributes, isSelected, bgImage } ) => {
		const classes = classnames( className, 'c-news__item' );
		const { quote, author, source, img, rel, target, url } = attributes;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';

		return (
			<div className={ classes }>
				<MediaUploadCheck fallback={ instructions }>
					<MediaUpload
						onSelect={ ( image ) => {
							setAttributes( {
								img: image.id,
							} );
						} }
						allowedTypes={ ALLOWED_MEDIA_TYPES }
						value={ img }
						render={ ( { open } ) => img ?
							<Fragment>
								{ isSelected && <Fragment>
									<Button isSecondary onClick={ () => {
										setAttributes( {
											img: undefined,
										} );
									} }>
										Remove Icon
									</Button></Fragment> }
							</Fragment> :
							( <Button isSecondary onClick={ open }>
								Add Icon
							</Button> ) }
					/>
					{ bgSrc && <img className="c-news__image u-fit-contain" src={ bgSrc } alt="" /> }
				</MediaUploadCheck>
				<RichText
					className="c-news__quote u-fw-400 o-h4 u-fs-normal"
					value={ quote }
					onChange={ ( content ) => setAttributes( { quote: content } ) }
					placeholder={ 'Enter Quote' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className="c-news__author u-c-green u-fw-700"
					value={ author }
					onChange={ ( content ) => setAttributes( { author: content } ) }
					placeholder={ 'Enter Author' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className="c-news__source u-c-green-lighter"
					value={ source }
					onChange={ ( content ) => setAttributes( { source: content } ) }
					placeholder={ 'Enter Source' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<URLPicker
					url={ url }
					setAttributes={ setAttributes }
					isSelected={ isSelected }
					opensInNewTab={ target === '_blank' }
					rel={ rel }
				/>
			</div>
		);
	} ),

	save: () => null,
} );
