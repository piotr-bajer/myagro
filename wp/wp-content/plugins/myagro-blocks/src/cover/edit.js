import BackgroundSelect from '../common/background-select';
import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { Component, Fragment } = wp.element;
const { InspectorControls, RichText, MediaUpload, MediaUploadCheck, withColors, PanelColorSettings } = wp.blockEditor;
const { PanelBody, TextControl, Button } = wp.components;
const { compose } = wp.compose;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the icon image, you need permission to upload media.' ) }</p>;

const youtubePatterns = [ {
	regex: /youtu\.be\/([\w\-_\?&=.]+)/i,
	url: 'https://www.youtube.com/embed/$1',
},
{
	regex: /youtube\.com(.+)v=([^&]+)(&([a-z0-9&=\-_]+))?/i,
	url: 'https://www.youtube.com/embed/$2?$4',
},
{
	regex: /youtube.com\/embed\/([a-z0-9\?&=\-_]+)/i,
	url: 'https://www.youtube.com/embed/$1',
} ];

const getUrl = function( pattern, url ) {
	const match = pattern.regex.exec( url );
	let newUrl = pattern.url;

	const replace = ( i ) => {
		newUrl = newUrl.replace( '$' + i, function() {
			return match[ i ] ? match[ i ] : '';
		} );
	};

	for ( let i = 0; i < match.length; i++ ) {
		replace( i );
	}

	return newUrl.replace( /\?$/, '' );
};

const matchPattern = ( url ) => {
	const pattern = youtubePatterns.filter( ( pt ) => {
		return pt.regex.test( url );
	} );

	if ( pattern.length > 0 ) {
		return getUrl( pattern[ 0 ], url );
	}
	return null;
};

class CoverEdit extends Component {
	render() {
		const { attributes, setAttributes, bgImage, iconImage, className, isSelected, textColor, titleColor, setTextColor, setTitleColor } = this.props;
		const { img, bg, title, url, text, target, rel, buttonText, embed, customTitleColor, customTextColor } = attributes;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';
		const iconSrc = iconImage && iconImage.source_url ? iconImage.source_url : '';
		const classes = classnames( className, 'c-cover o-wrapper-padding o-section u-text-center' );
		let styleName = className && className.match( /is-style-([^ ]+)/ );

		if ( styleName !== null && styleName.length > 0 ) {
			styleName = styleName[ 1 ];
		} else {
			styleName = 'default';
		}

		const youtubeUrl = embed && matchPattern( embed );

		const colorSettings = [
			{
				value: titleColor.color,
				onChange: setTitleColor,
				label: __( 'Title color' ),
			},
		];

		if ( styleName !== 'video' ) {
			colorSettings.push( {
				value: textColor.color,
				onChange: setTextColor,
				label: __( 'Text color' ),
			} );
		}

		const panelControls = <InspectorControls>
			{ <BackgroundSelect instructions={ instructions } bgImage={ bgImage } img={ bg } onRemoveImage={ () =>
				setAttributes( {
					bg: undefined,
				} ) } onUpdateImage={ image =>
				setAttributes( {
					bg: image.id,
				} )
			} /> }

			{ styleName === 'video' && <PanelBody title="Video URL" initialOpen={ true }>
				<div>
					<TextControl
						label="Video URL"
						value={ embed }
						onChange={ ( newURL ) => setAttributes( { embed: newURL } ) }
					/>
				</div>
			</PanelBody> }
			<PanelColorSettings
				title={ __( 'Color settings' ) }
				colorSettings={ colorSettings }
			/>
		</InspectorControls>;

		return (
			<Fragment>
				{ panelControls }
				<div className={ classes }>
					{ bgSrc && <img className="c-cover__bg u-fit-cover" src={ bgSrc } /> }
					<div className="c-cover__inner u-no-last-margin">
						{ styleName === 'default' && <MediaUploadCheck fallback={ instructions }>
							<MediaUpload
								onSelect={ ( image ) => {
									setAttributes( {
										img: image.id,
									} );
								} }
								allowedTypes={ [ 'image' ] }
								value={ img }
								render={ ( { open } ) => img ?
									<Fragment>
										{ isSelected && <Fragment>
											<Button isSecondary onClick={ () => {
												setAttributes( {
													img: undefined,
												} );
											} }>
												Remove Icon
											</Button></Fragment> }
									</Fragment> :
									( <Button isSecondary onClick={ open }>
										Add Icon
									</Button> ) }
							/>
							{ styleName === 'default' && iconSrc && <img className="c-cover__icon u-fit-contain" src={ iconSrc } alt="" /> }
						</MediaUploadCheck> }

						<RichText
							tagName="h2"
							className={ classnames( 'c-cover__title u-fw-300 u-ff-serif', styleName === 'quote' ? 'o-h2 u-fs-normal' : 'o-h1', titleColor && titleColor.class ) } style={ customTitleColor ? { color: customTitleColor } : {} }
							value={ title }
							onChange={ ( content ) => setAttributes( { title: content } ) }
							placeholder={ 'Enter title' }
							allowedFormats={ [ 'core/bold', 'core/italic', 'core/text-color' ] }
						/>

						{ styleName !== 'video' && <RichText
							tagName="div"
							className={ classnames( 'c-cover__text o-h4', textColor && textColor.class ) } style={ customTextColor ? { color: customTextColor } : {} }
							value={ text }
							onChange={ ( content ) => setAttributes( { text: content } ) }
							placeholder={ 'Enter text' }
							allowedFormats={ [ 'core/bold', 'core/italic', 'core/text-color' ] }
						/> }

						{ ( styleName === 'default' || styleName === 'text' ) && <RichText
							className={ 'o-btn o-btn--yellow o-btn--round o-btn--letter-spacing u-uppercase u-c-gray' }
							value={ buttonText }
							onChange={ ( content ) => setAttributes( { buttonText: content } ) }
							placeholder={ 'Add text' }
							withoutInteractiveFormatting
							allowedFormats={ [] }
						/> }

						{ ( styleName === 'default' || styleName === 'text' ) && <URLPicker
							url={ url }
							setAttributes={ setAttributes }
							isSelected={ isSelected }
							opensInNewTab={ target === '_blank' }
							rel={ rel }
						/> }

						{ styleName === 'video' && youtubeUrl && <div className="c-cover__embed">
							<iframe src={ youtubeUrl } frameBorder="0"
								allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
								allowFullScreen />
						</div> }
					</div>
				</div>
			</Fragment>
		);
	}
}

export default compose(
	withColors( { textColor: 'color' }, { titleColor: 'color' } ),
	withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img, bg } = props.attributes;

		return {
			bgImage: bg ? getMedia( bg ) : null,
			iconImage: img ? getMedia( img ) : null,
		};
	} ),
)( CoverEdit );
