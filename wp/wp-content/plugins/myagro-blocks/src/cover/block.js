import './editor.scss';
import './style.scss';
import edit from './edit';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/cover', {
	title: __( 'Cover Section' ),
	icon: 'format-image',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		caption: {
			type: 'string',
		},
		bg: {
			type: 'number',
		},
		img: {
			type: 'number',
		},
		anchor: {
			type: 'string',
		},
		buttonText: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
		embed: {
			type: 'string',
		},
		textColor: {
			type: 'string',
		},
		customTextColor: {
			type: 'string',
		},
		titleColor: {
			type: 'string',
		},
		customTitleColor: {
			type: 'string',
		},
	},
	supports: {
		html: false,
		anchor: true,
	},
	styles: [
		{
			name: 'default',
			label: __( 'Icon, text and CTA' ),
			isDefault: true,
		},
		{
			name: 'text',
			label: __( 'Text and CTA' ),
		},
		{
			name: 'video',
			label: __( 'Text and video' ),
		},
		{
			name: 'quote',
			label: __( 'Quote and caption' ),
		},
	],

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: edit,

	save: ( ) => {
		return null;
	},
} );
