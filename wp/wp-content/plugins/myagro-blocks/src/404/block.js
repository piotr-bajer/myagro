import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.blockEditor;

registerBlockType( 'myagro/block-404', {
	title: __( '404 error message' ),
	icon: 'warning',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		anchor: {
			type: 'string',
		},
	},
	supports: {
		html: false,
		anchor: true,
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { attributes, setAttributes, className } ) => {
		const classes = classnames( className, 'c-404 o-section o-wrapper-padding u-no-last-margin' );
		const { title, text } = attributes;

		return (
			<div className={ classes }>
				<RichText
					className="c-404__title o-h1 u-c-green u-fw-700 u-ff-serif"
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Enter Title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className="c-404__text u-fw-400 o-h4 o-generic-links"
					value={ text }
					onChange={ ( content ) => setAttributes( { text: content } ) }
					placeholder={ 'Enter Text' }
					allowedFormats={ [ 'core/bold', 'core/link' ] }
				/>
			</div>
		);
	},

	save: ( ) => {
		return null;
	},
} );
