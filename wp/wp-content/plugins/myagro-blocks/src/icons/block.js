import './editor.scss';
import './style.scss';
import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { InnerBlocks, RichText, MediaUpload, MediaUploadCheck, withColors, PanelColorSettings, InspectorControls } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { withSelect, select, dispatch } = wp.data;
const { Fragment } = wp.element;
const { Button } = wp.components;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;
const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'myagro/icons', {
	title: __( 'Icons' ),
	icon: 'grid-view',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		text: {
			type: 'string',
		},
		title: {
			type: 'string',
		},
		buttonText: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
		textColor: {
			type: 'string',
		},
		customTextColor: {
			type: 'string',
		},
		titleColor: {
			type: 'string',
		},
		customTitleColor: {
			type: 'string',
		},
	},
	styles: [
		{
			name: 'default',
			label: __( 'Icons and CTA' ),
			isDefault: true,
		},
		{
			name: 'icons',
			label: __( 'Just Icons' ),
		},
	],

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: withColors( { textColor: 'color' }, { titleColor: 'color' } )( withSelect( ( selectStore, props ) => {
		const { getMedia } = selectStore( 'core' );
		const { img } = props.attributes;

		return {
			iconImage: img ? getMedia( img ) : null,
		};
	} )( ( { className, attributes, setAttributes, isSelected, iconImage, clientId, textColor, titleColor, setTextColor, setTitleColor } ) => {
		const classes = classnames( className, 'c-icons o-wrapper-padding u-text-center o-section u-no-last-margin' );
		const { text, url, target, rel, title, img, buttonText, customTextColor, customTitleColor } = attributes;
		const iconSrc = iconImage && iconImage.source_url ? iconImage.source_url : '';
		let styleName = className && className.match( /is-style-([^ ]+)/ );

		if ( styleName !== null && styleName.length > 0 ) {
			styleName = styleName[ 1 ];
		} else {
			styleName = 'default';
		}

		const innerBlocks = select( 'core/block-editor' ).getBlocks( clientId );

		if ( innerBlocks && innerBlocks.length > 0 ) {
			innerBlocks.forEach( block => {
				dispatch( 'core/block-editor' ).updateBlockAttributes( block.clientId, { styleName } );
			} );
		}

		const colorSettings = [
			{
				value: titleColor.color,
				onChange: setTitleColor,
				label: __( 'Title color' ),
			},
		];

		if ( styleName !== 'icons' ) {
			colorSettings.push( {
				value: textColor.color,
				onChange: setTextColor,
				label: __( 'Text color' ),
			} );
		}

		const panelControls = <InspectorControls>
			<PanelColorSettings
				title={ __( 'Color settings' ) }
				colorSettings={ colorSettings }
			/>
		</InspectorControls>;

		return (
			<Fragment>
				{ panelControls }
				<div className={ classes }>
					{ styleName !== 'icons' && <MediaUploadCheck fallback={ instructions }>
						<MediaUpload
							onSelect={ ( image ) => {
								setAttributes( {
									img: image.id,
								} );
							} }
							allowedTypes={ ALLOWED_MEDIA_TYPES }
							value={ img }
							render={ ( { open } ) => img ?
								<Fragment>
									{ isSelected && <Fragment>
										<Button isSecondary onClick={ () => {
											setAttributes( {
												img: undefined,
											} );
										} }>
											Remove Icon
										</Button></Fragment> }
								</Fragment> :
								( <Button isSecondary onClick={ open }>
									Add Icon
								</Button> ) }
						/>
						{ styleName !== 'icons' && iconSrc && <img className="c-icons__image u-fit-contain" src={ iconSrc } alt="" /> }
					</MediaUploadCheck> }
					<RichText
						className={ classnames( 'c-icons__title o-h1 u-fw-300 u-ff-serif', titleColor && titleColor.class ? titleColor.class : 'u-c-green' ) } style={ customTitleColor ? { color: customTitleColor } : {} }
						value={ title }
						onChange={ ( content ) => setAttributes( { title: content } ) }
						placeholder={ 'Add title' }
						withoutInteractiveFormatting
						allowedFormats={ [ 'core/bold', 'core/italic', 'core/text-color' ] }
					/>
					{ styleName !== 'icons' && <RichText
						className={ classnames( 'c-icons__text o-h4', textColor && textColor.class ) } style={ customTextColor ? { color: customTextColor } : {} }
						value={ text }
						onChange={ ( content ) => setAttributes( { text: content } ) }
						placeholder={ 'Add text' }
						allowedFormats={ [ 'core/bold', 'core/italic', 'core/text-color' ] }
					/> }
					<InnerBlocks
						className="c-icons__items"
						allowedBlocks={ [ 'myagro/icons-item' ] }
						template={ [ [ 'myagro/icons-item' ] ] }
						__experimentalMoverDirection="horizontal"
					/>
					{ styleName === 'default' &&
					<Fragment>
						<RichText
							className={ 'o-btn o-btn--yellow o-btn--round o-btn--letter-spacing u-uppercase u-c-gray' }
							value={ buttonText }
							onChange={ ( content ) => setAttributes( { buttonText: content } ) }
							placeholder={ 'Add text' }
							withoutInteractiveFormatting
							allowedFormats={ [] }
						/>
						<URLPicker
							url={ url }
							setAttributes={ setAttributes }
							isSelected={ isSelected }
							opensInNewTab={ target === '_blank' }
							rel={ rel }
						/>
					</Fragment>
					}
				</div>
			</Fragment>
		);
	} ) ),

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
