import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, MediaUpload, MediaUploadCheck, withColors, PanelColorSettings, InspectorControls } = wp.blockEditor;
const { Fragment } = wp.element;
const { Button } = wp.components;
const { withSelect } = wp.data;
const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;
const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'myagro/icons-item', {
	title: __( 'Tile' ),
	parent: [ 'myagro/icons' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		title: {
			type: 'string',
		},
		styleName: {
			type: 'string',
			default: 'default',
		},
		text: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
		textColor: {
			type: 'string',
		},
		titleColor: {
			type: 'string',
		},
		customTextColor: {
			type: 'string',
		},
		customTitleColor: {
			type: 'string',
		},
	},

	edit: withColors( { textColor: 'color' }, { titleColor: 'color' } )( withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;
		return {
			iconImage: img ? getMedia( img ) : null,
		};
	} )( ( { className, attributes, setAttributes, isSelected, iconImage, textColor, titleColor, setTextColor, setTitleColor } ) => {
		const classes = classnames( className, 'c-icons__item' );
		const { title, text, img, styleName, customTextColor, customTitleColor } = attributes;
		const iconSrc = iconImage && iconImage.source_url ? iconImage.source_url : '';

		const colorSettings = [];

		if ( styleName === 'default' ) {
			colorSettings.push( {
				value: titleColor.color,
				onChange: setTitleColor,
				label: __( 'Title color' ),
			} );
		}

		colorSettings.push( {
			value: textColor.color,
			onChange: setTextColor,
			label: __( 'Text color' ),
		} );

		const panelControls = <InspectorControls>
			<PanelColorSettings
				title={ __( 'Color settings' ) }
				colorSettings={ colorSettings }
			/>
		</InspectorControls>;

		return (
			<Fragment>
				{ panelControls }
				<div className={ classes }>
					<MediaUploadCheck fallback={ instructions }>
						<MediaUpload
							onSelect={ ( image ) => {
								setAttributes( {
									img: image.id,
								} );
							} }
							allowedTypes={ ALLOWED_MEDIA_TYPES }
							value={ img }
							render={ ( { open } ) => img ?
								<Fragment>
									{ isSelected && <Fragment>
										<Button isSecondary onClick={ () => {
											setAttributes( {
												img: undefined,
											} );
										} }>
											Remove Icon
										</Button></Fragment> }
								</Fragment> :
								( <Button isSecondary onClick={ open }>
									Add Icon
								</Button> ) }
						/>
						{ iconSrc && <img className="c-icons__item-image u-fit-contain" src={ iconSrc } alt="" /> }
					</MediaUploadCheck>
					{ styleName === 'default' && <RichText
						className={ classnames( 'c-icons__item-title u-fw-400 u-ff-serif o-h3', titleColor && titleColor.class ? titleColor.class : 'u-c-green' ) } style={ customTitleColor ? { color: customTitleColor } : {} }
						value={ title }
						onChange={ ( content ) => setAttributes( { title: content } ) }
						placeholder={ 'Enter Title' }
						allowedFormats={ [ 'core/bold', 'core/italic', 'core/text-color' ] }
					/> }
					<RichText
						className={ classnames( 'c-icons__item-text', textColor && textColor.class ) } style={ customTextColor ? { color: customTextColor } : {} }
						value={ text }
						onChange={ ( content ) => setAttributes( { text: content } ) }
						placeholder={ 'Enter Text' }
						allowedFormats={ [ 'core/bold', 'core/italic', 'core/text-color' ] }
					/>
				</div>
			</Fragment>
		);
	} ) ),

	save: () => null,
} );
