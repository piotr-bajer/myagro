import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.blockEditor;
const { Fragment } = wp.element;

registerBlockType( 'myagro/links-item', {
	title: __( 'Link' ),
	parent: [ 'myagro/links' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		text: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},

	edit: ( { className, attributes, setAttributes, isSelected } ) => {
		const classes = classnames( className, 'c-links__item o-btn o-btn--green-lighter u-fw-700' );
		const { text, url, target, rel } = attributes;

		return (
			<Fragment>
				<RichText
					className={ classes }
					value={ text }
					onChange={ ( content ) => setAttributes( { text: content } ) }
					placeholder={ 'Add text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<URLPicker
					url={ url }
					setAttributes={ setAttributes }
					isSelected={ isSelected }
					opensInNewTab={ target === '_blank' }
					rel={ rel }
				/>
			</Fragment>
		);
	},

	save: () => null,
} );
