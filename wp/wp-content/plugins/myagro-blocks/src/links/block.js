import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/links', {
	title: __( 'Links' ),
	icon: 'admin-links',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		anchor: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		title: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { className, attributes, setAttributes } ) => {
		const classes = classnames( className, 'c-links o-wrapper-padding-narrow o-section has-beige-lighter-background-color u-text-center' );
		const { title, text } = attributes;
		return (
			<div className={ classes }>
				<RichText
					className={ 'c-links__title o-h1 u-c-green u-fw-300 u-ff-serif u-text-center' }
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Add title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className={ 'c-links__text o-h4' }
					value={ text }
					onChange={ ( content ) => setAttributes( { text: content } ) }
					placeholder={ 'Add title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<InnerBlocks
					className="c-links__items"
					allowedBlocks={ [ 'myagro/links-item' ] }
					template={ [ [ 'myagro/links-item' ] ] }
					__experimentalMoverDirection="horizontal"
				/>
			</div>
		);
	},

	save: ( ) => <InnerBlocks.Content />,
} );
