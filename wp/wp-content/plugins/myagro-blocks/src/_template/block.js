import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/block-myagro-blocks', {
	title: __( 'myagro-blocks - CGB Block' ),
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],

	edit: ( { className } ) => {
		const classes = classnames( className, 'c-buttons' );
		return null;
	},

	save: ( ) => {
		return null;
	},
} );
