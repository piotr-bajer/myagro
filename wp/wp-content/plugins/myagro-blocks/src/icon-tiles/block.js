import './editor.scss';
import './style.scss';
import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/icon-tiles', {
	title: __( 'Icon Tiles' ),
	// icon: 'grid-view',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		text: {
			type: 'string',
		},
		title: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { className, attributes, setAttributes, isSelected } ) => {
		const classes = classnames( className, 'c-icon-tiles o-wrapper-padding-wide u-text-center o-section has-beige-lighter-background-color' );
		const { text, url, target, rel, title } = attributes;

		return (
			<div className={ classes }>
				<RichText
					className={ 'c-icon-tiles__title o-h1 u-c-green u-fw-300 u-ff-serif' }
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Add title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<InnerBlocks
					className="c-icon-tiles__items"
					allowedBlocks={ [ 'myagro/icon-tiles-item' ] }
					template={ [ [ 'myagro/icon-tiles-item' ] ] }
					__experimentalMoverDirection="horizontal"
				/>
				<RichText
					className={ 'o-btn o-btn--yellow o-btn--round o-btn--letter-spacing u-uppercase u-c-gray' }
					value={ text }
					onChange={ ( content ) => setAttributes( { text: content } ) }
					placeholder={ 'Add text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<URLPicker
					url={ url }
					setAttributes={ setAttributes }
					isSelected={ isSelected }
					opensInNewTab={ target === '_blank' }
					rel={ rel }
				/>
			</div>
		);
	},

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
