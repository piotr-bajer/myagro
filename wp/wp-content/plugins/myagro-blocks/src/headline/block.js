import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.blockEditor;

registerBlockType( 'myagro/headline', {
	title: __( 'Headline' ),
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		subtitle: {
			type: 'subtitle',
		},
		anchor: {
			type: 'string',
		},
	},
	supports: {
		html: false,
		anchor: true,
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { attributes, setAttributes, className } ) => {
		const classes = classnames( className, 'c-headline o-section u-text-center o-wrapper-padding' );
		const { title, subtitle } = attributes;

		return (
			<div className={ classes }>
				<RichText
					className="c-headline__title o-h1 u-c-green u-fw-300 u-ff-serif"
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Enter Title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className="c-headline__subtitle u-fw-400 o-h4"
					value={ subtitle }
					onChange={ ( content ) => setAttributes( { subtitle: content } ) }
					placeholder={ 'Enter Title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
			</div>
		);
	},

	save: ( ) => {
		return null;
	},
} );
