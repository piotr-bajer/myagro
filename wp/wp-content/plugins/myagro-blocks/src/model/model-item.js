import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, MediaUpload, MediaUploadCheck } = wp.blockEditor;
const { Fragment } = wp.element;
const { Button } = wp.components;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;
const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'myagro/model-item', {
	title: __( 'Tile' ),
	parent: [ 'myagro/model' ],
	category: 'myagro',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		title: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
	},

	edit: withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;
		return {
			bgImage: img ? getMedia( img ) : null,
		};
	} )( ( { className, attributes, setAttributes, isSelected, bgImage } ) => {
		const classes = classnames( className, 'c-model__item' );
		const { title, text, img } = attributes;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';

		return ( <div className={ classes }>
			<div className="c-model__media">
				{ bgSrc && <img className="c-model__image u-fit-contain" src={ bgSrc } alt="" /> }
				<MediaUploadCheck fallback={ instructions }>
					<MediaUpload
						onSelect={ ( image ) => {
							setAttributes( {
								img: image.id,
							} );
						} }
						allowedTypes={ ALLOWED_MEDIA_TYPES }
						value={ img }
						render={ ( { open } ) => img ?
							<Fragment>
								{ isSelected && <Fragment>
									<Button isSecondary onClick={ () => {
										setAttributes( {
											img: undefined,
										} );
									} }>
										Remove Image
									</Button></Fragment> }
							</Fragment> :
							( <Button isSecondary onClick={ open }>
								Add Image
							</Button> ) }
					/>
				</MediaUploadCheck>
			</div>
			<RichText
				className="c-model__item-title o-h4 u-ff-serif u-c-green"
				value={ title }
				onChange={ ( content ) => setAttributes( { title: content } ) }
				placeholder={ 'Enter Tile Title' }
				withoutInteractiveFormatting
				allowedFormats={ [] }
			/>
			<div className="c-model__separator" />
			<RichText
				className="c-model__item-text o-h6"
				value={ text }
				onChange={ ( content ) => setAttributes( { text: content } ) }
				placeholder={ 'Enter Tile Text' }
				withoutInteractiveFormatting
				allowedFormats={ [ 'core/bold' ] }
			/>
		</div>
		);
	} ),

	save: () => null,
} );
