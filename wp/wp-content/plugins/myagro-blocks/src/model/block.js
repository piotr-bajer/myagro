import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'myagro/model', {
	title: __( 'Model' ),
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		footer: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { className, attributes, setAttributes } ) => {
		const classes = classnames( className, 'c-model o-wrapper-padding-wide o-section has-beige-lighter-background-color u-no-last-margin' );
		const { title, text, footer } = attributes;

		return (
			<div className={ classes }>
				<RichText
					className={ 'c-model__title o-h1 u-fw-300 u-ff-serif u-text-center u-c-green' }
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Add title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className={ 'c-model__text o-h4 u-text-center' }
					value={ text }
					onChange={ ( content ) => setAttributes( { text: content } ) }
					placeholder={ 'Add text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				{ <InnerBlocks
					className="c-model__items"
					allowedBlocks={ [ 'myagro/model-item' ] }
					template={ [ [ 'myagro/model-item' ] ] }
					__experimentalMoverDirection="horizontal"
				/> }
				<RichText
					className={ 'c-model__footer u-fw-700 u-c-green o-h6 u-text-center' }
					value={ footer }
					onChange={ ( content ) => setAttributes( { footer: content } ) }
					placeholder={ 'Add footer note' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
			</div>
		);
	},

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
