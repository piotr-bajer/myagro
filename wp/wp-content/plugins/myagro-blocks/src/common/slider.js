import BackgroundSelect from './background-select';

const { withSelect, dispatch } = wp.data;
const { InspectorControls } = wp.blockEditor;
const { __ } = wp.i18n;

const instructions = <p>{ __( 'To edit the icon, you need permission to upload media.' ) }</p>;

export function isVisible( component ) {
	return withSelect( ( select, props ) => {
		const { clientId } = props;
		const { getBlockIndex, getBlockRootClientId, getBlockAttributes } = select( 'core/block-editor' );
		const rootClientId = getBlockRootClientId( clientId );
		const rootAttributes = getBlockAttributes( rootClientId );
		const blockIndex = getBlockIndex( clientId, rootClientId );
		return {
			isVisible: rootAttributes.currentSlideUid === clientId || ( blockIndex === 0 && rootAttributes.currentSlideUid === null ),
		};
	} )( component );
}

export default ( component ) => {
	return withSelect( ( select, ownProps ) => {
		const { clientId } = ownProps;
		const {
			getBlock,
		} = select( 'core/block-editor' );
		const block = getBlock( clientId );
		const { getMedia } = select( 'core' );
		const { getBlockIndex, getBlockAttributes } = select( 'core/block-editor' );
		const blockIndex = getBlockIndex( ownProps.attributes.currentSlideUid, clientId );

		if ( ( ownProps.attributes.currentSlideUid === null || blockIndex < 0 ) && block.innerBlocks.length > 0 ) {
			ownProps.attributes.currentSlideUid = block.innerBlocks[ 0 ].clientId;
		}

		let iconControls = null;

		if ( ownProps.attributes.currentSlideUid && block.innerBlocks && block.innerBlocks.length ) {
			const currentSlideAttrs = getBlockAttributes( ownProps.attributes.currentSlideUid );
			const iconId = currentSlideAttrs && currentSlideAttrs.icon ? currentSlideAttrs.icon : null;
			const iconObject = iconId ? getMedia( iconId ) : null;

			iconControls = <InspectorControls>
				{ <BackgroundSelect imageName="Icon" instructions={ instructions } bgImage={ iconObject } img={ iconId } onRemoveImage={ () =>
					dispatch( 'core/block-editor' ).updateBlockAttributes( ownProps.attributes.currentSlideUid, { icon: undefined } )
				} onUpdateImage={ image =>
					dispatch( 'core/block-editor' ).updateBlockAttributes( ownProps.attributes.currentSlideUid, { icon: image.id } )
				} /> }
			</InspectorControls>;
		}

		return {
			blockIndex: Math.max( blockIndex, 0 ),
			blockUids: block.innerBlocks.map( b => b.clientId ),
			innerBlocks: block.innerBlocks,
			iconControls,
		};
	} )( component );
};
