const { MediaUpload, MediaUploadCheck } = wp.blockEditor;
const { PanelBody, Button, ResponsiveWrapper, Spinner } = wp.components;

const ALLOWED_MEDIA_TYPES = [ 'image' ];

export default ( { onUpdateImage, onRemoveImage, instructions, bgImage, img, imageName = 'Background' } ) => {
	return <PanelBody
		title={ imageName + ' settings' }
		initialOpen={ true }
	>
		<div>
			<MediaUploadCheck fallback={ instructions }>
				<MediaUpload
					title={ imageName + ' image' }
					onSelect={ onUpdateImage }
					allowedTypes={ ALLOWED_MEDIA_TYPES }
					value={ img }
					render={ ( { open } ) => (
						<Button
							className={ img ? 'img-preview-button' : '' }
							onClick={ open }>
							{ ! img && 'Set ' + imageName + ' image' }
							{ !! img && ! bgImage && <Spinner /> }
							{ !! img && bgImage &&
							<ResponsiveWrapper
								naturalWidth={ bgImage.media_details.width }
								naturalHeight={ bgImage.media_details.height }
							>
								<img src={ bgImage.source_url } alt={ imageName + ' image' } />
							</ResponsiveWrapper>
							}
						</Button>
					) }
				/>
			</MediaUploadCheck>
			{ !! img && bgImage &&
			<MediaUploadCheck>
				<MediaUpload
					title={ imageName + ' image' }
					onSelect={ onUpdateImage }
					allowedTypes={ ALLOWED_MEDIA_TYPES }
					value={ img }
					render={ ( { open } ) => (
						<Button onClick={ open } isSecondary isLarge>
							Replace { imageName } image
						</Button>
					) }
				/>
			</MediaUploadCheck>
			}
			{ !! img &&
			<MediaUploadCheck>
				<Button onClick={ onRemoveImage } isLink isDestructive>
					Remove { imageName } image
				</Button>
			</MediaUploadCheck>
			}
		</div>
	</PanelBody>;
};
