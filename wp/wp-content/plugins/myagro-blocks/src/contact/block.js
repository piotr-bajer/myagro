import './editor.scss';
import './style.scss';
import classnames from 'classnames';
import './store.js';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;
const { InspectorControls, InnerBlocks, RichText } = wp.blockEditor;
const { Placeholder, SelectControl, PanelBody, Disabled, TextControl } = wp.components;
const { withSelect } = wp.data;
const ServerSideRender = wp.serverSideRender;

registerBlockType( 'myagro/contact', {
	title: __( 'Contact' ),
	icon: 'email',
	category: 'myagro',
	keywords: [
		__( 'myAgro' ),
	],
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		facebookLink: {
			type: 'string',
		},
		twitterLink: {
			type: 'string',
		},
		contactFormId: {
			type: 'number',
		},
		anchor: {
			type: 'string',
		},
		texts: [],
	},
	supports: {
		html: false,
		anchor: true,
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: withSelect( ( select ) => {
		const data = select( 'myagro/contact-form-7' ).getForms();
		const forms = data ? data.map( ( form ) => {
			return { label: form.title, value: form.id };
		} ) : [];

		return { forms: [ { label: 'Select contact form', value: 0 }, ...forms ] };
	} )(
		( { className, attributes, setAttributes, forms } ) => {
			const classes = classnames( className, 'c-contact o-wrapper o-section' );
			const { title, facebookLink, twitterLink, contactFormId } = attributes;

			const onChange = value => {
				setAttributes( { contactFormId: parseInt( value, 10 ) } );
			};

			const select = <SelectControl
				label="Contact form"
				value={ contactFormId }
				options={ forms }
				onChange={ onChange }
			/>;

			const form = contactFormId > 0 ? <Disabled>
				<ServerSideRender
					block="myagro/shortcode"
					attributes={ { shortcode: ` [contact-form-7 id="${ contactFormId }"]` } }
				/>
			</Disabled> : <Placeholder label="Select contact form">{ select }</Placeholder>;

			return (
				<Fragment>
					<InspectorControls>
						<PanelBody title={ __( 'Block settings' ) }>
							{ select }
							<TextControl
								label={ __( 'Twitter link' ) }
								value={ twitterLink || '' }
								onChange={ content => {
									setAttributes( { twitterLink: content } );
								} }
							/>
							<TextControl
								label={ __( 'Facebook link' ) }
								value={ facebookLink || '' }
								onChange={ content => {
									setAttributes( { facebookLink: content } );
								} }
							/>
						</PanelBody>
					</InspectorControls>
					<div className={ classes }>
						<RichText
							className={ 'c-contact__title o-h1 u-c-green u-fw-300 u-ff-serif u-text-center' }
							value={ title }
							onChange={ ( content ) => setAttributes( { title: content } ) }
							placeholder={ 'Add title' }
							withoutInteractiveFormatting
							allowedFormats={ [] }
						/>
						<div className="c-contact__columns">
							<div className="c-contact__form-wrapper">
								{ form }
							</div>
							<div className="c-contact__content u-no-last-margin">
								<InnerBlocks
									className="u-no-last-margin"
									allowedBlocks={ [ 'core/paragraph' ] }
									template={ [ [ 'core/paragraph' ] ] }
								/>
								{ ( facebookLink || twitterLink ) && <div className="c-contact__social">
									{ twitterLink && <span className="c-contact__social-link"><svg className="c-contact__social-icon"><use xlinkHref="#svg-twitter" /></svg></span> }
									{ facebookLink && <span className="c-contact__social-link"><svg className="c-contact__social-icon"><use xlinkHref="#svg-facebook" /></svg></span> }
								</div> }
							</div>
						</div>

					</div>
				</Fragment>
			);
		} ),

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
