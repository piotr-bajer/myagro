const { apiRequest } = wp;
const { registerStore, dispatch } = wp.data;

registerStore( 'myagro/contact-form-7', {
	reducer( state = {}, action ) {
		switch ( action.type ) {
			case 'MYAGRO_CONTACT_FORMS_7':
				return {
					forms: action.data,
				};
		}

		return state;
	},

	actions: {
		setForms( result ) {
			return {
				type: 'MYAGRO_CONTACT_FORMS_7',
				data: result,
			};
		},
	},

	selectors: {
		getForms( result ) {
			if ( typeof result === 'object' && result.forms !== undefined ) {
				return result.forms;
			}
		},
	},

	resolvers: {
		async getForms() {
			const result = await apiRequest( { path: 'contact-form-7/v1/contact-forms/' } );
			dispatch( 'myagro/contact-form-7' ).setForms( result );
		},
	},
} );
