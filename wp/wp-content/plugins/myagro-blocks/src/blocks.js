/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

import './hero/block';
import './video-text/block';
import './icon-tiles/icon-tiles-item';
import './icon-tiles/block';
import './cover/block';
import './posts-carousel/posts-carousel-item';
import './posts-carousel/block';
import './news/news-item';
import './news/block';
import './headline/block';
import './icons/icons-item';
import './icons/block';
import './story-cards/story-cards-item';
import './story-cards/block';
import './links/links-item';
import './links/block';
import './slideshow/slideshow-item';
import './slideshow/block';
import './values/values-item';
import './values/block';
import './jobs/jobs-item';
import './jobs/jobs-region';
import './jobs/block';
import './team-gallery/team-gallery-item';
import './team-gallery/block';
import './members/block';
import './stats/stats-stat';
import './stats/stats-item';
import './stats/block';
import './model/model-item';
import './model/block';
import './posts-list/block';
import './contact/block';
import './404/block';
