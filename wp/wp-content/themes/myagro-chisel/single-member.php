<?php
/**
 * The Template for displaying all member posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package myagro
 */

global $post;

$context = \Timber\Timber::get_context();

$context['memberData'] = \Chisel\Helpers::getMemberData(get_the_ID());

if ( post_password_required( $post->ID ) ) {
	\Timber\Timber::render( 'single-password.twig', $context );
} else {
	\Timber\Timber::render( array( 'single-member.twig', 'single.twig' ), $context );
}
