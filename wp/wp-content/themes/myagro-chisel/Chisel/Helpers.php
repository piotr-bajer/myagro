<?php

namespace Chisel;

use Timber\Timber;

/**
 * Class Helpers
 * @package Chisel
 *
 * Defines helper methods used by Chisel
 */
class Helpers
{
	static $manifest;

	public static function isTimberActivated()
	{
		return class_exists('Timber\\Timber');
	}

	public static function addTimberAdminNotice()
	{
		add_action('admin_notices',
			function () {
				echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
			}
		);
	}

	public static function setChiselEnv()
	{
		if (isset($_SERVER['HTTP_X_CHISEL_PROXY'])) {
			define('CHISEL_DEV_ENV', true);
		}
	}

	public static function getMemberData($id = null, $taxonomy = null)
	{
		if ($id === null) {
			$id = get_the_ID();
		}

		$id = get_post_field('ID', $id);

		if ($id < 0) {
			return false;
		}


		$raw_content = get_post_field('post_content', $id);
		$content     = apply_filters('the_content', $raw_content);

		return array(
			'position'  => get_field('position', $id),
			'thumbnail' => get_post_thumbnail_id($id),
			'title'     => get_the_title($id),
			'content'   => $content,
			'taxonomy'  => $taxonomy,
			'id'        => $id,
		);
	}

	public static function getMembersBlockData()
	{
		$member_groups = get_terms(array(
			'post_type'  => 'member',
			'taxonomy'   => 'member-group',
			'hide_empty' => true,
		));

		$data = array('groups' => $member_groups, 'cards' => array());

		foreach ($member_groups as $term) {
			$data['cards'][$term->term_id] = static::getMembersData(array(), array(
				'member-group' => array($term->term_id),
			));
		}

		return $data;
	}

	public static function getMembersData($parameters = array(), $filters = array())
	{
		$filter_taxonomies = array(
			'member-group' => 'By type',
		);

		$filters    = $filters ? $filters : static::parseFilterValues(array_keys($filter_taxonomies));
		$parameters = wp_parse_args($parameters, array(
			'posts_per_page' => -1,
			'orderby'        => array('menu_order' => 'ASC', 'date' => 'DESC', 'ID' => 'DESC'),
			'order'          => ''
		));
		$post_data  = static::getFilteredPosts('member', $filters, $parameters);
		$posts      = array();

		foreach ($post_data['ids'] as $post_id) {
			$group = ! empty($filters['member-group']) ? $filters['member-group'] : wp_get_post_terms($post_id,
				'member-group', array('number' => 1, 'fields' => 'ids'));

			$posts[] = array(
				'thumbnail' => get_post_thumbnail_id($post_id),
				'title'     => get_the_title($post_id),
				'id'        => $post_id,
				'group_id' => (! is_wp_error($group) && $group[0] ? $group[0] : ''),
				'position'  => get_field('position', $post_id),
			);
		}

		$taxonomies = static::getFilterTerms($filter_taxonomies, $filters, 'member');

		return array(
			'total'      => $post_data['total'],
			'taxonomies' => $taxonomies,
			'posts'      => $posts,
		);
	}

	public static function getMediaData($parameters = array(), $filters = array())
	{
		$parameters = array_merge($_REQUEST, $parameters);

		if (empty($parameters['offset'])) {
			$parameters['offset'] = 0;
		}

		$page                 = max(1, (int)$parameters['offset']);
		$parameters['offset'] = ($page - 1) * 5;

		if ( ! empty($parameters['search'])) {
			$parameters['search'] = stripslashes($parameters['search']);
		} else {
			$parameters['search'] = '';
		}

		$filter_taxonomies = array(
			'type-media'   => 'Filter by Type',
			'author-media' => 'Filter by Author',
		);
		$filters           = $filters ? $filters : static::parseFilterValues(array_keys($filter_taxonomies));
		$post_data         = static::getFilteredPosts('media-post', $filters, $parameters);
		$posts             = array();
		$media_page_id     = get_option('page_for_media');

		if (empty($media_page_id)) {
			$media_page_id = get_the_ID();
		}

		$base_url = get_the_permalink($media_page_id);

		foreach ($post_data['ids'] as $post_id) {
			$type_term = wp_get_post_terms($post_id, 'type-media', array('number' => 1));
			$authors   = wp_get_post_terms($post_id, 'author-media', array('fields' => 'id=>name'));
			$type      = '';

			if ( ! is_wp_error($type_term) && $type_term) {
				$type = $type_term[0]->name;
			}

			$authors_parsed = array();

			foreach ($authors as $author_id => $author_name) {
				$authors_parsed[] = array(
					'url'  => add_query_arg('author-media', $author_id, $base_url),
					'name' => $author_name
				);
			}

			$posts[] = array(
				'thumbnail' => get_post_thumbnail_id($post_id),
				'title'     => get_the_title($post_id),
				'id'        => $post_id,
				'url'       => get_the_permalink($post_id),
				'type'      => $type,
				'authors'   => $authors_parsed,
				'date'      => get_the_date('', $post_id),
			);
		}

		unset($filter_taxonomies['author-media']);
		$taxonomies = static::getFilterTerms($filter_taxonomies, $filters, 'media-post');


		$add_args = array();

		if ( ! empty($parameters['search'])) {
			$add_args['search'] = $parameters['search'];
		}

		if ( ! empty($parameters['type-media'])) {
			$add_args['type-media'] = $parameters['type-media'];
		}

		$pagination = paginate_links(array(
			'base'      => $base_url . '%_%',
			'format'    => (stripos($base_url, '?') === false ? '?' : '&') . 'offset=%#%',
			'total'     => ceil($post_data['total'] / 5),
			'current'   => max(1, (int)$page),
			'end_size'  => 2,
			'mid_size'  => 2,
			'prev_text' => '<',
			'next_text' => '>',
			'add_args'  => $add_args,
		));

		return array(
			'total'      => $post_data['total'],
			'taxonomies' => $taxonomies,
			'posts'      => $posts,
			'search'     => ! empty($parameters['search']) ? $parameters['search'] : '',
			'pagination' => $pagination,
		);
	}

	private static function parseFilterValues($filter_names)
	{
		$filters = array();

		foreach ($filter_names as $filter_name) {
			$temp_values = ! empty($_REQUEST[$filter_name]) ? explode(',', $_REQUEST[$filter_name]) : array();
			$values      = array();

			foreach ($temp_values as $value) {
				$value = intval($value);

				if ($value) {
					$values[] = $value;
				}
			}

			if ($values) {
				$filters[$filter_name] = $values;
			}
		}

		return $filters;
	}

	private static function getFilteredPosts($post_name, $filters, $parameters = array())
	{
		$default_parameters = array(
			'offset'         => ! empty($_REQUEST['offset']) ? (int)$_REQUEST['offset'] : 0,
			'orderby'        => ! empty($_REQUEST['orderby']) ? sanitize_key($_REQUEST['orderby']) : array(
				'date' => 'DESC',
				'ID'   => 'DESC'
			),
			'order'          => ! empty($_REQUEST['order']) ? sanitize_key($_REQUEST['order']) : array('DESC', 'DESC'),
			'posts_per_page' => ! empty($_REQUEST['posts_per_page']) ? (int)$_REQUEST['posts_per_page'] : 5,
			'search'         => ! empty($_REQUEST['search']) ? sanitize_text_field($_REQUEST['search']) : '',
		);

		$parameters = wp_parse_args($parameters, $default_parameters);

		$tax_query = array('relation' => 'and');

		foreach ($filters as $filter_name => $filter_values) {
			$tax_query[] = array(
				'taxonomy' => $filter_name,
				'terms'    => $filter_values,
			);
		}

		$query_settings = array(
			'post_status'    => 'publish',
			'post_type'      => $post_name,
			'fields'         => 'ids',
			'posts_per_page' => $parameters['posts_per_page'],
			'offset'         => $parameters['offset'],
			's'              => $parameters['search'],
			'orderby'        => $parameters['orderby'],
			'order'          => $parameters['order'],
			'tax_query'      => $tax_query,
		);

		$query = new \WP_Query($query_settings);
		$ids   = $query->get_posts();

		return array('ids' => $ids, 'total' => $query->found_posts);
	}

	private static function getFilterTerms($filter_taxonomies, $filters, $post_type)
	{
		$terms            = get_terms(array(
			'post_type'  => $post_type,
			'taxonomy'   => array_keys($filter_taxonomies),
			'hide_empty' => true,
		));
		$taxonomies       = array();
		$selected_filters = array();

		foreach ($filters as $filter_values) {
			$selected_filters = array_merge($filter_values);
		}

		foreach ($filter_taxonomies as $taxonomy_name => $taxonomy_label) {
			$taxonomies[$taxonomy_name] = array('title' => $taxonomy_label, 'terms' => array(), 'selected' => array());
		}

		if ($terms) {
			foreach ($terms as $term) {
				$selected                               = in_array($term->term_id, $selected_filters);
				$taxonomies[$term->taxonomy]['terms'][] = array(
					'id'       => $term->term_id,
					'title'    => $term->name,
					'selected' => $selected,
				);
			}
		}

		return $taxonomies;
	}

	public static function prepareRecruiterboxBlockData()
	{
		$jobs = get_option('myagro_jobs', array());

		$jobs_regions = array();

		foreach($jobs as $region => $jobs_data) {
			$job_lines = array();

			foreach($jobs_data as $job_data) {
				$job_lines[] = \Timber\Timber::compile('blocks/myagro/jobs-item.twig', array(
					'attrs' => array(
						'url' => $job_data['url'],
						'rel' => 'noopener noreferrer',
						'target' => '_blank',
						'title' => $job_data['title'],
						'type' => $job_data['time'],
						'location' => $job_data['remote_allowed'] ? 'Remote/On Site' : 'On Site',
					)
				));
			}

			$jobs_regions[] = \Timber\Timber::compile('blocks/myagro/jobs-region.twig', array(
				'innerBlocks' => $jobs_data,
				'blockContent' => implode(PHP_EOL, $job_lines),
				'attrs' => array(
					'title' => $region,
				),
			));
		}

		return implode(PHP_EOL, $jobs_regions);
	}
}
