<?php

namespace Chisel;

class Gutenberg
{
	public function __construct()
	{
		$ChiselTwig = Settings::getExtensionInstance('ChiselTwig');

		add_filter('render_block', [$this, 'render_block'], 10, 2);
		add_filter('render_block_data', [$this, 'render_block_data'], 10, 2);
		add_action('enqueue_block_editor_assets', array($this, 'enqueue_block_editor_assets'));
		$editor_styles_path = get_template_directory_uri() . '/' . ltrim(str_replace(get_template_directory_uri(), '',
				$ChiselTwig->revisionedPath('styles/editor.css')), '/');
		add_editor_style($editor_styles_path);
		add_action('init', array($this, 'register_dynamic_blocks'), 11);
		add_action('init', array($this, 'set_default_templates'));
		//add_filter('allowed_block_types', array($this, 'allowed_block_types'));
	}

	public function allowed_block_types($allowed_blocks)
	{
		return $allowed_blocks;
	}

	public function enqueue_block_editor_assets()
	{
		$ChiselTwig = Settings::getExtensionInstance('ChiselTwig');

		wp_enqueue_script(
			'myagro-editor-scripts',
			$ChiselTwig->revisionedPath('scripts/editor.bundle.js'),
			array('wp-blocks', 'wp-dom-ready', 'wp-edit-post')
		);
	}

	public function render_block($block_content, $block)
	{
		$relative_path = 'templates/blocks/' . $block['blockName'] . '.twig';
		$template_file = get_template_directory() . '/' . $relative_path;

		if (file_exists($template_file)) {
			$block['blockContent'] = $block_content;
			$block_content         = \Timber\Timber::compile($relative_path, $block);
		} elseif (stripos($block['blockName'], 'myagro/') === false
		          && preg_match('/(<[^>]* (id="?\'?([^"\']+)"?\'?)[^>]*>)/is', $block_content, $m)) {
			if ( ! preg_match('/(<input|<select|<textarea|o-anchor)/i', $m[1])) {
				$block_content = \Timber\Timber::compile('partials/anchor.twig',
						array('id' => $m[3])) . str_replace($m[2],
						'', $block_content);
			}
		} elseif ($block['blockName'] === 'core/paragraph') {
			if (stripos($block_content, '<p></p>') !== false) {
				$block_content = '';
			} else {
				$block_content = str_replace('<p>', '<p class="o-generic-links">', $block_content);

				if (stripos($block_content, '<br></p>') !== false) {
					$block_content = str_replace('<br></p>', '<br><br></p>', $block_content);
				}
			}
		} elseif ($block['blockName'] === 'core/list') {
			if (stripos($block_content, '<ul></ul>') !== false) {
				$block_content = '';
			} else {
				$block_content = str_replace('<ul>', '<ul class="o-generic-links">', $block_content);
			}
		}

		return $block_content;
	}

	public function render_block_data($block, $source_block)
	{
		if ( ! isset($block['attrs']['className'])) {
			$block['attrs']['className'] = '';
		}

		if (preg_match('/is-style-([^ ]+)/i', $block['attrs']['className'], $m) > 0) {
			$block['attrs']['styleName'] = $m[1];
		} else {
			$block['attrs']['styleName'] = 'default';
		}

		if ($block['blockName'] === 'myagro/members') {
			$block['attrs'] = array_merge($block['attrs'], \Chisel\Helpers::getMembersBlockData());
		} elseif ($block['blockName'] === 'myagro/posts-list') {
			$block['attrs'] = array_merge($block['attrs'], \Chisel\Helpers::getMediaData());
		} elseif ($block['blockName'] === 'myagro/recruiterbox') {
			$block['attrs'] = array_merge($block['attrs'], array('blockContent' => Helpers::prepareRecruiterboxBlockData()));
		} elseif ($block['blockName'] === 'myagro/jobs' && !empty($block['attrs']['type']) && $block['attrs']['type'] === 'auto') {
			$block['attrs'] = array_merge($block['attrs'], array('blockContent' => Helpers::prepareRecruiterboxBlockData()));
		}

		return $block;
	}

	public function register_dynamic_blocks()
	{
		register_block_type(
			'myagro/members',
			array(
				'attributes'      => array(
					'title'  => array(
						'type' => 'string',
					),
					'anchor' => array(
						'type' => 'string',
					),
				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);
		register_block_type(
			'myagro/posts-list',
			array(
				'attributes'      => array(
					'title'  => array(
						'type' => 'string',
					),
					'anchor' => array(
						'type' => 'string',
					),
				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);

		register_block_type(
			'myagro/shortcode',
			array(
				'attributes'      => array(
					'anchor'    => array(
						'type' => 'string',
					),
					'shortcode' => array(
						'type' => 'string',
					),
				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);

		register_block_type(
			'myagro/recruiterbox',
			array(
				'attributes'      => array(
					'title'  => array(
						'type' => 'string',
					),
					'anchor' => array(
						'type' => 'string',
					),
					'isEditor' => array(
						'type' => 'boolean',
					),
				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);

	}

	public function set_default_templates()
	{

	}
}
