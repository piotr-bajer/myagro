<?php

namespace Chisel;

use Timber\Menu;
use Timber\Timber;

/**
 * Class Site
 * @package Chisel
 *
 * Use this class to setup whole site related configuration
 */
class Site extends \Timber\Site
{
	/**
	 * Site constructor.
	 */
	public function __construct()
	{
		// set default twig templates directory
		Timber::$dirname = Settings::TEMPLATES_DIR;

		$this->chiselInit();
		$this->postsSorting();

		add_action('myagro_cron_job', array($this, 'myAgroCronJob'));

		if ( ! wp_next_scheduled('myagro_cron_job')) {
			wp_schedule_event(time(), 'hourly', 'myagro_cron_job');
		}

		parent::__construct();
	}

	/**
	 * Initiate chisel configuration.
	 */
	public function chiselInit()
	{
		add_filter('timber_context', array($this, 'addToContext'));
		add_filter('wp_head', array($this, 'addToHeader'));
		add_filter('Timber\PostClassMap', array('\Chisel\Post', 'overrideTimberPostClass'));

		register_nav_menus([
			'navigation-top' => __('Main Menu'),
			'footer-1'       => __('Quick Links'),
		]);
	}

	/**
	 * You can add custom global data to twig context
	 *
	 * @param array $context
	 *
	 * @return array
	 */
	public static function addToContext($context)
	{
		$context['main_nav']    = new Menu('navigation-top');
		$context['quick_links'] = new Menu('footer-1');

		$context['post']    = Timber::get_post();
		$context['site_js'] = array(
			'ajax_url' => admin_url('admin-ajax.php'),
			'post_id'  => get_the_ID(),
		);

		if (function_exists('get_field')) {
			$context['header_buttons']                = get_field('header_buttons', 'options');
			$context['footer_addresses']              = get_field('footer_addresses', 'options');
			$context['footer_social_links']           = get_field('footer_social_links', 'options');
			$context['footer_copyright']              = get_field('footer_copyright', 'options');
			$context['newsletter_show_popup']         = get_field('newsletter_show_popup', 'options');
			$context['newsletter_mailchimp_list_url'] = get_field('newsletter_mailchimp_list_url', 'options');
		}

		if ( ! empty($context['newsletter_show_popup']) && ! empty($context['newsletter_mailchimp_list_url'])) {
			$context['newsletter_popup'] = \Timber\Timber::compile('components/newsletter-popup.twig',
				array('newsletter_mailchimp_list_url' => $context['newsletter_mailchimp_list_url']));
		}

		return $context;
	}

	public static function addToHeader()
	{
		echo '<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  			  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
              <link rel="dns-prefetch" href="https://fonts.googleapis.com">';
	}

	public function postsSorting()
	{
		add_filter('get_previous_post_sort', array($this, 'get_post_sort'), 10, 3);
		add_filter('get_next_post_sort', array($this, 'get_post_sort'), 10, 3);
	}


	public function get_post_sort($sort, $post, $order)
	{
		return 'ORDER BY p.menu_order ' . $order . ', p.post_date ' . $order . ', p.ID ' . $order . ' LIMIT 1';
	}


	public function myAgroCronJob()
	{
		$key = get_field('recruiterbox_key', 'option');

		if (empty($key)) {
			return;
		}

		$request = wp_remote_get('https://api.recruiterbox.com/v2/openings?offset=0&limit=100&is_private=false&state=Published',
			array(
				'timeout'     => 10,
				'redirection' => 5,
				'httpversion' => '1.1',
				'user-agent'  => 'myAgro',
				'headers'     => array(
					'accept-language' => 'en-US',
					'Content-Type'    => 'application/json',
					'Authorization'   => 'Basic ' . base64_encode($key),
				),
			));

		if ( ! is_wp_error($request)) {
			$body = json_decode(wp_remote_retrieve_body($request), true);

			$jobs  = array();
			$times = array(
				'contract'  => 'Contract',
				'full_time' => 'Full Time',
				'part_time' => 'Part Time',
			);

			$states = array();

			if ( ! empty($body)) {
				foreach ($body['objects'] as $job) {
					$time           = $times[$job['position_type']];
					$title          = $job['title'];
					$url            = $job['hosted_url'];
					$remote_allowed = $job['is_remote_allowed'];
					$city           = trim($job['location']['city']);
					$country        = trim($job['location']['country']);

					if (stripos($city, ',') !== false) {
						$city_split = explode(',', $city);
						$city       = trim($city_split[0]);

						if (empty($country)) {
							$country = trim(array_pop($city_split));
						}
					}

					$location = $city . ($country ? ', ' . $country : '');

					$jobs[$location][] = array(
						'city'           => $city,
						'country'        => $country,
						'time'           => $time,
						'title'          => $title,
						'url'            => $url,
						'remote_allowed' => $remote_allowed,
					);

					$states[] = $job['state'];
				}
			}

			uksort($jobs, function ($a, $b) {
				$location_a = explode(',', $a);
				$location_b = explode(',', $b);
				$count_a = count($location_a);
				$count_b = count($location_b);


				if($count_a === $count_b && $count_a > 1) {
					if($location_a[1] === $location_b[1]) {
						if($location_a[0] === $location_b[0]) {
							return 0;
						}

						return strcmp($location_a[0], $location_b[0]);
					}

					return strcmp($location_a[1], $location_b[1]);
				} else {
					return $count_a < $count_b ? -1 : 1;
				}
			});

			update_option('myagro_jobs', $jobs, false);
		}
	}
}
