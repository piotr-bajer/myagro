<?php

namespace Chisel\Extensions;

use Chisel\Settings;

/**
 * Class Theme
 * Use this class to extend theme functionality
 * @package Chisel\Extensions
 */
class Theme implements ChiselExtension
{
	public function extend()
	{
		$this->addThemeSupports();

		add_action('admin_enqueue_scripts', array($this, 'load_admin_style'));
		add_action('admin_head', array($this, 'admin_head'));
	}

	private function addThemeSupports()
	{
		add_theme_support('post-formats');
		add_theme_support('post-thumbnails');
		add_theme_support('menus');
		add_theme_support('title-tag');
		add_theme_support('align-wide');
		add_theme_support('wp-block-styles');
		add_theme_support('editor-color-palette', array(
			array(
				'name'  => __('Black'),
				'slug'  => 'black',
				'color' => '#000000',
			),
			array(
				'name'  => __('White'),
				'slug'  => 'white',
				'color' => '#ffffff',
			),
			array(
				'name'  => __('Green'),
				'slug'  => 'green',
				'color' => '#176d2a',
			),
			array(
				'name'  => __('Yellow'),
				'slug'  => 'yellow',
				'color' => '#fbd906',
			),
			array(
				'name'  => __('Gray'),
				'slug'  => 'gray',
				'color' => '#5a5a4b',
			),
		));

		add_theme_support('editor-styles');

		add_theme_support('editor-font-sizes', array(
			array(
				'name' => __('H1'),
				'size' => 45,
				'slug' => 'h1'
			),
			array(
				'name' => __('H2'),
				'size' => 35,
				'slug' => 'h2'
			),
			array(
				'name' => __('H3'),
				'size' => 30,
				'slug' => 'h3'
			),
			array(
				'name' => __('H4'),
				'size' => 26,
				'slug' => 'h4'
			),
			array(
				'name' => __('H5'),
				'size' => 22,
				'slug' => 'h5'
			),
			array(
				'name' => __('H6'),
				'size' => 20,
				'slug' => 'h6'
			),
		));

		add_theme_support('responsive-embeds');
	}

	public function load_admin_style()
	{
		$ChiselTwig         = Settings::getExtensionInstance('ChiselTwig');
		$admin_styles_path = get_template_directory_uri() . '/' . ltrim(str_replace(get_template_directory_uri(), '',
				$ChiselTwig->revisionedPath('styles/admin.css')), '/');
		wp_enqueue_style('myagro-admin-css', $admin_styles_path);
	}

	public function admin_head()
	{
		echo '<link href="https://fonts.googleapis.com/css2?family=Zilla+Slab:wght@300;400;500&display=swap" rel="stylesheet">' . PHP_EOL;
	}
}
