<?php

namespace Chisel\Extensions;

class AdminPanel implements ChiselExtension
{
	public function extend()
	{
		add_action('admin_init', array($this, 'admin_init'));
		add_action('admin_init', array($this, 'register_settings'));
		add_action('rest_api_init', array($this, 'register_settings'));
		add_filter('display_post_states', array($this, 'display_post_states'), 10, 2);
	}

	public function register_settings()
	{
		$options = array('media', 'members', '404');

		foreach($options as $option) {
			register_setting(
				'reading',
				'page_for_' . $option,
				array(
					'type'              => 'integer',
					'sanitize_callback' => 'intval',
					'default'           => null,
					'show_in_rest'      => true,
				)
			);
		}
	}

	public function admin_init()
	{
		add_settings_field(
			'page_for_media',
			__('Page for Media', 'myagro'),
			array($this, 'page_for_media_callback'),
			'reading',
			'default',
			array('label_for' => 'page_for_media')
		);

		add_settings_field(
			'page_for_members',
			__('Page for Members', 'myagro'),
			array($this, 'page_for_members_callback'),
			'reading',
			'default',
			array('label_for' => 'page_for_members')
		);

		add_settings_field(
			'page_for_404',
			__('Page for 404 Error', 'myagro'),
			array($this, 'page_for_404_callback'),
			'reading',
			'default',
			array('label_for' => 'page_for_404')
		);
	}

	public function page_for_media_callback()
	{
		$this->page_callback('media');
	}

	public function page_for_members_callback()
	{
		$this->page_callback('members');
	}

	public function page_for_404_callback()
	{
		$this->page_callback('404');
	}

	private function page_callback($slug)
	{
		$page_id = get_option('page_for_' . $slug);

		$items = get_posts(array(
			'posts_per_page' => -1,
			'orderby'        => 'name',
			'order'          => 'ASC',
			'post_type'      => 'page',
		));

		echo '<select id="page_for_' . $slug . '" name="page_for_' . $slug . '">';
		echo '<option value="0">' . __('— Select —', 'wordpress') . '</option>';

		foreach ($items as $item) {
			$selected = ($page_id == $item->ID) ? 'selected="selected"' : '';

			echo '<option value="' . $item->ID . '" ' . $selected . '>' . $item->post_title . '</option>';
		}

		echo '</select>';
	}

	public function display_post_states($post_states, $post)
	{
		$media_page_id   = get_option('page_for_media');
		$members_page_id = get_option('page_for_members');
		$error_page_id   = get_option('page_for_404');

		if ($post->ID == $media_page_id) {
			$post_states[] = __('Media Page', 'myagro');
		}

		if ($post->ID == $members_page_id) {
			$post_states[] = __('Members Page', 'myagro');
		}

		if ($post->ID == $error_page_id) {
			$post_states[] = __('404 Error Page', 'myagro');
		}

		return $post_states;
	}
}
