<?php

namespace Chisel\Extensions;

/**
 * Class DataType
 * Use this class to register custom post types and taxonomies
 * @package Chisel\Extensions
 */
class DataType implements ChiselExtension
{
	public function extend()
	{
		add_action('init', array($this, 'registerPostTypes'));
		add_action('init', array($this, 'registerTaxonomies'));

		if (function_exists('acf_add_options_page')) {
			acf_add_options_page(array(
				'page_title' => 'Theme Options',
				'menu_title' => 'Theme Options',
				'menu_slug'  => 'theme-options',
				'capability' => 'edit_posts',
				'redirect'   => false
			));
		}
	}

	/**
	 * Use this method to register custom post types
	 */
	public function registerPostTypes()
	{
		foreach (
			array(
				'member'   => array(
					'singular'            => __('Member'),
					'plural'              => __('Members'),
					'publicly_queryable'  => true,
					'exclude_from_search' => true,
					'rewrite'             => array('slug' => 'member', 'with_front' => true),
					'menu_position'       => 4,
					'has_archive'         => false,
					'supports'            => array('title', 'revisions', 'thumbnail', 'editor'),
					'show_in_rest'        => true,
				),
				'media-post'   => array(
					'singular'            => __('Media Post'),
					'plural'              => __('Media Posts'),
					'publicly_queryable'  => true,
					'exclude_from_search' => false,
					'rewrite'             => array('slug' => 'media-post', 'with_front' => true),
					'menu_position'       => 4,
					'has_archive'         => false,
					'supports'            => array('title', 'revisions', 'thumbnail', 'editor'),
					'show_in_rest'        => true,
				),
			) as $id => $data
		) {
			$this->addPostType($id, $data);
		}
	}

	/**
	 * Use this method to register custom taxonomies
	 */
	public function registerTaxonomies()
	{
		foreach (
			array(
				'member-group'          => array(
					'singular'           => __('Group'),
					'plural'             => __('Groups'),
					'rewrite'            => array('slug' => 'member-group', 'with_front' => true),
					'posts'              => array('member'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
				'type-media'          => array(
					'singular'           => __('Type'),
					'plural'             => __('Types'),
					'rewrite'            => array('slug' => 'type-media', 'with_front' => true),
					'posts'              => array('media-post'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
				'author-media'          => array(
					'singular'           => __('Author'),
					'plural'             => __('Authors'),
					'rewrite'            => array('slug' => 'author-media', 'with_front' => true),
					'posts'              => array('media-post'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
			) as $id => $data
		) {
			$this->addTaxonomy($id, $data);
		}
	}

	private function addPostType($post_type, array $settings = array())
	{
		$defaults = array(
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array('slug' => $post_type, 'with_front' => false),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 15,
			'can_export'         => true,
			'show_in_nav_menus'  => true,
			'taxonomies'         => array(),
			'supports'           => array('title', 'author', 'thumbnail', 'excerpt', 'comments'),
		);

		$args = wp_parse_args($settings, $defaults);

		if (!empty($settings['plural'])) {
			$args['labels'] = array(
				'name'              => __(esc_attr("{$settings['plural']}"), 'post type general name'),
				'singular_name'     => __(esc_attr("{$settings['singular']}"), 'post type singular name'),
				'parent_item_colon' => '',
				'menu_name'         => $settings['plural'],
			);
		}

		register_post_type($post_type, $args);
	}

	private function addTaxonomy($taxonomy, array $settings = array())
	{
		$defaults = array(
			'public'            => true,
			'hierarchical'      => true,
			'query_var'         => true,
			'show_admin_column' => true,
			'sort'              => false,
		);

		$args = wp_parse_args($settings, $defaults);

		if (!empty($settings['plural'])) {

			$labels = array(
				'name'                       => $settings['plural'],
				'singular_name'              => $settings['singular'],
				'menu_name'                  => isset($settings['menu_name']) ? $settings['menu_name'] : $settings['plural'],
				'search_items'               => 'Search ' . $settings['plural'],
				'all_items'                  => 'All ' . $settings['plural'],
				'edit_item'                  => 'Edit ' . $settings['singular'],
				'update_item'                => 'Update ' . $settings['singular'],
				'add_new_item'               => 'Add new ' . $settings['singular'],
				'new_item_name'              => 'New ' . $settings['singular'],
				'parent_item'                => 'Parent ' . $settings['singular'],
				'parent_item_colon'          => 'Parent ' . $settings['singular'],
				'separate_items_with_commas' => 'Separate tags with commas ' . $settings['plural'],
				'add_or_remove_items'        => 'Add or remove ' . $settings['plural'],
				'choose_from_most_used'      => 'Choose from most used ' . $settings['plural'],
			);

			if (false === $args['hierarchical']) {
				$labels['popular_items'] = 'Popular ' . $settings['plural'];
				if ( ! isset($settings['update_count_callback'])) {
					$args['update_count_callback'] = '_update_post_term_count';
				}
			}

			$args['labels'] = $labels;
		}

		register_taxonomy($taxonomy, $settings['posts'], $args);
	}
}
