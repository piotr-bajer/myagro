<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package myagro
 */

global $post;

$context = \Timber\Timber::get_context();
$media_page_id = get_option('page_for_media');
$context['page_for_media'] = $media_page_id;
$type = wp_get_post_terms(get_the_ID(), 'type-media', array('number' => 1, 'fields' => 'names'));
$context['type'] = $type ? $type[0] : '';
$context['date'] = get_the_date('', get_the_ID());
$authors   = wp_get_post_terms(get_the_ID(), 'author-media', array('fields' => 'id=>name'));
$authors_parsed = array();
$base_url = get_the_permalink($media_page_id);

foreach ($authors as $author_id => $author_name) {
	$authors_parsed[] = array('url'  => add_query_arg('author-media', $author_id, $base_url),
	                          'name' => $author_name
	);
}

$context['authors'] = $authors_parsed;
$context['url'] = get_the_permalink();
$context['url_encoded'] = urlencode(get_the_permalink());

if ( post_password_required( $post->ID ) ) {
	\Timber\Timber::render( 'single-password.twig', $context );
} else {
	\Timber\Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}
