<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package myagro
 */

$context = \Timber\Timber::get_context();
$error_page_id = get_option('page_for_404');
$content = $error_page_id ? apply_filters('the_content', get_post_field('post_content', $error_page_id)) : '';
$context['content'] = $content;
\Timber\Timber::render( '404.twig', $context );
