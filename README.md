# MyAgro

In order to review the code please investigate these folders:

- src/scripts
- src/styles
- wp/wp-content/plugins/myagro-blocks/src
- wp/wp-content/themes/myagro-chisel

MyAgro is a project created with Chisel. Please check out Chisel documentation at [https://www.getchisel.co/docs/](www.getchisel.co/docs/).

